
#ifndef VTLS_HASH_H
#define VTLS_HASH_H

#include <openssl/evp.h>
#include <openssl/ossl_typ.h>

#include "../Constants.h"
#include "../VtlsError.h"
#include "../utils/ByteBuffer.h"

class Hash {
public:
    Hash();
    virtual ~Hash();

    int Init(int alg);
    int Update(const char* b, int bLen);
    int Finish();

    int QuickCalc(int alg, const char* b, int bLen);

    ByteBuffer* Result() { return &m_Result; }

    static int GetHashSize(int hashAlg);
    static const EVP_MD* GetEvpMD(int hashAlg);

private:
    EVP_MD_CTX* m_Ctx;
    ByteBuffer  m_Result;
};

#endif //VTLS_HASH_H
