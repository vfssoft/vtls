
#ifndef VTLS_CONNSTATE_H
#define VTLS_CONNSTATE_H

#include "../utils/TypeDefs.h"
#include "../utils/ByteBuffer.h"


class ConnState {
public:
    ByteBuffer   Mac;
    ByteBuffer   Key;
    ByteBuffer   IV;
    bool         Enc;
    UINT64       SeqNo;

    ConnState() {
        Reset();
    }
    void Reset() {
        Mac.Reset();
        Key.Reset();
        IV.Reset();
        Enc = false;
        SeqNo = 0;
    }
    void Set(ConnState* other) {
        Mac.Set(&other->Mac);
        Key.Set(&other->Key);
        IV.Set(&other->IV);
        Enc = other->Enc;
        SeqNo = other->SeqNo;
    }
};


#endif //VTLS_CONNSTATE_H
