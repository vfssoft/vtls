
#include "Cert.h"

// ref: https://zakird.com/2013/10/13/certificate-parsing-with-openssl

Cert::Cert(): m_Handle(NULL) {

}
Cert::~Cert() {
    Reset();
}

int Cert::Load(const char* der, int len) {
    Reset();

    m_Handle = d2i_X509(NULL, (const unsigned char**)&der, len);
    if (m_Handle == NULL) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}
int Cert::Load(const char* pem) {
    Reset();

    BIO* bio = BIO_new(BIO_s_mem());
    BIO_write(bio, pem, strlen(pem));
    m_Handle = PEM_read_bio_X509(bio, NULL, NULL, NULL);
    BIO_free(bio);

    if (m_Handle == NULL) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}

int Cert::Version() {
    return X509_get_version(m_Handle) + 1;
}
ByteBuffer* Cert::SubjectName() {
    X509_NAME* name = X509_get_subject_name(m_Handle);
    char* subject = X509_NAME_oneline(name, NULL, 0);
    m_Subject.SetStr(subject);
    OPENSSL_free(subject);

    return &m_Subject;
}
ByteBuffer* Cert::IssuerName() {
    X509_NAME* name = X509_get_issuer_name(m_Handle);
    char* issuer = X509_NAME_oneline(name, NULL, 0);
    m_Issuer.SetStr(issuer);
    OPENSSL_free(issuer);

    return &m_Issuer;
}
ByteBuffer* Cert::SerialNumber() {
    ASN1_INTEGER *serialNum = X509_get_serialNumber(m_Handle);
    BIGNUM *bn = ASN1_INTEGER_to_BN(serialNum, NULL);
    char *str = BN_bn2dec(bn);
    m_SerialNum.SetStr(str);
    BN_free(bn);
    OPENSSL_free(str);

    return &m_SerialNum;
}
int Cert::SignatureAlgorithmNID() {
    return X509_get_signature_nid(m_Handle);
}
UINT64 Cert::NotBefore() {
    struct tm time;
    ASN1_TIME* asnTime = X509_get_notBefore(m_Handle);
    ASN1_TIME_to_tm(asnTime, &time);
    return mktime(&time);
}
UINT64 Cert::NotAfter() {
    struct tm time;
    ASN1_TIME* asnTime = X509_get_notAfter(m_Handle);
    ASN1_TIME_to_tm(asnTime, &time);
    return mktime(&time);
}

EVP_PKEY* Cert::PublicKey() {
    return X509_get_pubkey(m_Handle);
}


void Cert::Reset() {
    if (m_Handle) {
        X509_free(m_Handle);
    }
    m_Handle = NULL;
}
