
#include "CipherSuite.h"

void CipherSuite::Init(UINT16 id, int keyLen, int macLen, int ivLen, int flags) {
    ID = id;
    KeyLen = keyLen;
    MacLen = macLen;
    IVLen = ivLen;
    Flags = flags;
}


CipherSuites::CipherSuites() {
    Ciphers.Add()->Init(TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, 16, 0, 4, CIPHER_SUITE_FLAG_ECDHE | CIPHER_SUITE_FLAG_TLS12);
    Ciphers.Add()->Init(TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, 16, 0, 4, CIPHER_SUITE_FLAG_ECDHE | CIPHER_SUITE_FLAG_ECSIGN|CIPHER_SUITE_FLAG_TLS12);
    Ciphers.Add()->Init(TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, 32, 0, 4, CIPHER_SUITE_FLAG_ECDHE | CIPHER_SUITE_FLAG_TLS12 | CIPHER_SUITE_FLAG_SHA384);
    Ciphers.Add()->Init(TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, 32, 0, 4, CIPHER_SUITE_FLAG_ECDHE | CIPHER_SUITE_FLAG_ECSIGN|CIPHER_SUITE_FLAG_TLS12 | CIPHER_SUITE_FLAG_SHA384);
}

CipherSuite* CipherSuites::Find(int ID) {
    for (int i = 0; i < Ciphers.Size(); i++) {
        CipherSuite* c = Ciphers.Get(i);
        if (c->ID == ID) {
            return c;
        }
    }
    return NULL;
}