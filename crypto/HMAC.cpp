
#include "HMAC.h"

Hmac::Hmac(): m_Ctx(NULL) {

}
Hmac::~Hmac() {
    Reset();
}

int Hmac::Init(int alg, const char* key, int keyLen) {
    int err = 0;

    Reset();
    m_Ctx = HMAC_CTX_new();
    const EVP_MD* md = Hash::GetEvpMD(alg);
    if (md == NULL) {
        return VTLS_ERR_CRYPT0;
    }

    if (!HMAC_Init_ex(m_Ctx, key, keyLen, md, NULL)) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}
int Hmac::Update(const char* data, int dataLen) {
    if (!HMAC_Update(m_Ctx, (const unsigned char*)data, dataLen)) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}
int Hmac::Final() {
    unsigned int hmacSize = HMAC_size(m_Ctx);
    m_Result.SetLength(hmacSize);
    if (!HMAC_Final(m_Ctx, (unsigned char*)m_Result.Ptr(), &hmacSize)) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}


void Hmac::Reset() {
    m_Result.Reset();
    if (m_Ctx) {
        HMAC_CTX_free(m_Ctx);
    }
    m_Ctx = NULL;
}

int Hmac::QuickCalc(int alg, const char* key, int keyLen, const char* data, int dataLen) {
    int err = 0;
    if (err = Init(alg, key, keyLen)) return err;
    if (err = Update(data, dataLen)) return err;
    if (err = Final()) return err;
    return 0;
}