
#ifndef VTLS_CIPHERSUITE_H
#define VTLS_CIPHERSUITE_H

#include "../utils/TypeDefs.h"
#include "../utils/Array.h"
#include "../Constants.h"

#define CIPHER_SUITE_FLAG_ECDHE  1
#define CIPHER_SUITE_FLAG_ECSIGN 2
#define CIPHER_SUITE_FLAG_TLS12  4
#define CIPHER_SUITE_FLAG_SHA384 8

class CipherSuite {
public:
    UINT16 ID;
    int    KeyLen;
    int    MacLen;
    int    IVLen;
    int    Flags;

    void Init(UINT16 id, int keyLen, int macLen, int ivLen, int flags);

};

class CipherSuites {
public:
    CipherSuites();

    CipherSuite* Find(int ID);

    Array<CipherSuite> Ciphers;
};


#endif //VTLS_CIPHERSUITE_H
