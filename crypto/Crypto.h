
#ifndef VTLS_CRYPTO_H
#define VTLS_CRYPTO_H

#include <openssl/rand.h>

class Crypto {
public:
    static void RandomBytes(char* buf, int bufLen);
};


#endif //VTLS_CRYPTO_H
