

#ifndef VTLS_HMAC_H
#define VTLS_HMAC_H

#include <openssl/ossl_typ.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>

#include "../utils/ByteBuffer.h"
#include "Hash.h"
#include "../VtlsError.h"

class Hmac {
public:
    Hmac();
    ~Hmac();

    int Init(int alg, const char* key, int keyLen);
    int Update(const char* data, int dataLen);
    int Final();
    void Reset();
    ByteBuffer* Result() { return &m_Result; }

    int QuickCalc(int alg, const char* key, int keyLen, const char* data, int dataLen);

private:
    HMAC_CTX*   m_Ctx;
    ByteBuffer  m_Result;
};


#endif //VTLS_HMAC_H
