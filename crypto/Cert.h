
#ifndef VTLS_CERT_H
#define VTLS_CERT_H

#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/asn1.h>

#include "../utils/TypeDefs.h"
#include "../VtlsError.h"
#include "../utils/ByteBuffer.h"

class Cert {
public:
    Cert();
    virtual ~Cert();

    int Load(const char* der, int len);
    int Load(const char* pem);

    int Version();
    ByteBuffer* SubjectName();
    ByteBuffer* IssuerName();
    ByteBuffer* SerialNumber();
    int SignatureAlgorithmNID();
    UINT64 NotBefore();
    UINT64 NotAfter();

    EVP_PKEY* PublicKey();

    void Reset();

private:
    X509* m_Handle;

    ByteBuffer m_Subject;
    ByteBuffer m_Issuer;
    ByteBuffer m_SerialNum;
};


#endif //VTLS_CERT_H
