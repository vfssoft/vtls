
#ifndef VTLS_PRF_H
#define VTLS_PRF_H

#include "../utils/ByteBuffer.h"
#include "Hash.h"
#include "HMAC.h"
#include "CipherSuite.h"

class PRF {
public:
    int GenMasterFromPreMaster(
            CipherSuite* suite,
            const char* preMaster, int lenPreMaster,
            const char* clientRandom, int lenClientRandom,
            const char* serverRandom, int lenServerRandom,
            ByteBuffer* master
    );

    int GenKeys(
            CipherSuite* suite,
            const char* master, int lenMaster,
            const char* clientRandom, int lenClientRandom,
            const char* serverRandom, int lenServerRandom,
            int macLen, int keyLen, int ivLen,
            ByteBuffer* clientMac, ByteBuffer* serverMac,
            ByteBuffer* clientKey, ByteBuffer* serverKey,
            ByteBuffer* clientIV, ByteBuffer* serverIV
    );

    int GenVerifyData(CipherSuite* suite, ByteBuffer* handshakes, const char* master, int lenMaster, const char* label, ByteBuffer* result);

private:
    int P_hash(const char* secret, int lenSecret, const char* seed, int lenSeed, int hashAlg, ByteBuffer* result);
    int PRF12(const char* secret, int lenSecret, const char* label, int lenLabel, const char* seed, int lenSeed, int hashAlg, ByteBuffer* result);

    int GetHashAlg(CipherSuite* suite);
};


#endif //VTLS_PRF_H
