
#ifndef VTLS_KEYAGREEMENT_H
#define VTLS_KEYAGREEMENT_H

#include <openssl/ec.h>
#include <openssl/ecdh.h>
#include <openssl/evp.h>

#include "../messages/ServerKeyExchange.h"
#include "../VtlsError.h"

class KeyAgreement {
public:
    KeyAgreement();
    virtual ~KeyAgreement();

    int ProcessServerKeyExchange(int namedCurve, const char* pubKey, int lenPubKey);
    int GenerateClientKeyExchange(ByteBuffer* ret);
    ByteBuffer* SharedKey() { return &m_SharedKey; }

    void Reset();

private:
    int GenerateECDHKey(int curveID, EC_KEY** key);

private:
    EC_KEY*    m_OurEcKey;
    ByteBuffer m_SharedKey;
};


#endif //VTLS_KEYAGREEMENT_H
