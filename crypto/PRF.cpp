
#include "PRF.h"

#include <cstring>

#define MASTER_SECRET_LENGTH  48
#define VERIFY_DATA_LENGTH    12
#define MASTER_SECRET_LABEL   "master secret"
#define KEY_EXPANSION_LABEL   "key expansion"

int PRF::GenMasterFromPreMaster(
        CipherSuite* suite,
        const char* preMaster, int lenPreMaster,
        const char* clientRandom, int lenClientRandom,
        const char* serverRandom, int lenServerRandom,
        ByteBuffer* master
) {
    ByteBuffer seed;
    seed.Append(clientRandom, lenClientRandom);
    seed.Append(serverRandom, lenServerRandom);

    master->SetLength(MASTER_SECRET_LENGTH);

    return PRF12(preMaster, lenPreMaster, MASTER_SECRET_LABEL, strlen(MASTER_SECRET_LABEL), seed.Ptr(), seed.Length(), GetHashAlg(suite), master);
}

int PRF::GenKeys(
        CipherSuite* suite,
        const char* master, int lenMaster,
        const char* clientRandom, int lenClientRandom,
        const char* serverRandom, int lenServerRandom,
        int macLen, int keyLen, int ivLen,
        ByteBuffer* clientMac, ByteBuffer* serverMac,
        ByteBuffer* clientKey, ByteBuffer* serverKey,
        ByteBuffer* clientIV, ByteBuffer* serverIV
) {
    int err = 0;
    ByteBuffer seed;
    int n = 2 * macLen + 2 * keyLen + 2 * ivLen;
    ByteBuffer keyMaterial;

    keyMaterial.SetLength(n);
    seed.Append(serverRandom, lenServerRandom);
    seed.Append(clientRandom, lenClientRandom);

    err = PRF12(master, lenMaster, KEY_EXPANSION_LABEL, strlen(KEY_EXPANSION_LABEL), seed.Ptr(), seed.Length(), GetHashAlg(suite), &keyMaterial);
    if (err) return err;

    clientMac->Set(keyMaterial.Ptr(), macLen);
    serverMac->Set(keyMaterial.Ptr() + macLen, macLen);

    clientKey->Set(keyMaterial.Ptr() + 2 * macLen, keyLen);
    serverKey->Set(keyMaterial.Ptr() + 2 * macLen + keyLen, keyLen);

    clientIV->Set(keyMaterial.Ptr() + 2 * macLen + 2 * keyLen, ivLen);
    serverIV->Set(keyMaterial.Ptr() + 2 * macLen + 2 * keyLen + ivLen, ivLen);

    return 0;
}


int PRF::GenVerifyData(CipherSuite* suite, ByteBuffer* handshakes, const char* master, int lenMaster, const char* label, ByteBuffer* result) {
    Hash hash;
    hash.QuickCalc(GetHashAlg(suite), handshakes->Ptr(), handshakes->Length());
    result->SetLength(VERIFY_DATA_LENGTH);
    return PRF12(master, lenMaster, label, strlen(label), hash.Result()->Ptr(), hash.Result()->Length(), GetHashAlg(suite), result);
}

int PRF::P_hash(const char* secret, int lenSecret, const char* seed, int lenSeed, int hashAlg, ByteBuffer* result) {
    int err = 0;
    Hmac hmac;
    ByteBuffer a;

    if (err = hmac.Init(hashAlg, secret, lenSecret)) return err;
    if (err = hmac.Update(seed, lenSeed)) return err;
    if (err = hmac.Final()) return err;
    a.Set(hmac.Result()->Ptr(), hmac.Result()->Length());

    int j = 0;
    while (j < result->Length()) {
        if (err = hmac.Init(hashAlg, secret, lenSecret)) return err;
        if (err = hmac.Update(a.Ptr(), a.Length())) return err;
        if (err = hmac.Update(seed, lenSeed)) return err;
        if (err = hmac.Final()) return err;

        int lenToCopy = hmac.Result()->Length() + j > result->Length() ? result->Length() - j : hmac.Result()->Length();
        result->Set(j, hmac.Result()->Ptr(), lenToCopy);
        j += hmac.Result()->Length();

        if (err = hmac.Init(hashAlg, secret, lenSecret)) return err;
        if (err = hmac.Update(a.Ptr(), a.Length())) return err;
        if (err = hmac.Final()) return err;
        a.Set(hmac.Result()->Ptr(), hmac.Result()->Length());
    }
    return 0;
}

int PRF::PRF12(const char* secret, int lenSecret, const char* label, int lenLabel, const char* seed, int lenSeed, int hashAlg, ByteBuffer* result) {
    ByteBuffer labelSeed;
    labelSeed.Append(label, lenLabel);
    labelSeed.Append(seed, lenSeed);
    return P_hash(secret, lenSecret, labelSeed.Ptr(), labelSeed.Length(), hashAlg, result);
}

int PRF::GetHashAlg(CipherSuite* suite) {
    if (suite->Flags & CIPHER_SUITE_FLAG_SHA384) {
        return HASH_ALG_SHA384;
    } else {
        return HASH_ALG_SHA256;
    }
}