
#include "KeyAgreement.h"


KeyAgreement::KeyAgreement() {
    m_OurEcKey = NULL;
}
KeyAgreement::~KeyAgreement() {
    Reset();
}
void KeyAgreement::Reset() {
    if (m_OurEcKey) {
        EC_KEY_free(m_OurEcKey);
    }
    m_OurEcKey = NULL;
    m_SharedKey.Reset();
}

int KeyAgreement::ProcessServerKeyExchange(int namedCurve, const char* pubKey, int lenPubKey) {
    int err = 0;
    int fieldSize = 0;
    const EC_GROUP* ecGroup = NULL;
    EC_POINT* peerPoint = NULL;

    if (err = GenerateECDHKey(namedCurve, &m_OurEcKey)) return err;

    ecGroup = EC_KEY_get0_group(m_OurEcKey);
    peerPoint = EC_POINT_new(ecGroup);
    if (1 != EC_POINT_oct2point(ecGroup, peerPoint, (const unsigned char*)(pubKey), lenPubKey, NULL)) {
        return VTLS_ERR_CRYPT0;
    }

    fieldSize = EC_GROUP_get_degree(ecGroup);
    m_SharedKey.SetLength((fieldSize + 7) / 8);
    err = ECDH_compute_key(m_SharedKey.Ptr(), m_SharedKey.Length(), peerPoint, m_OurEcKey, NULL);
    if (err <= 0) {
        return VTLS_ERR_CRYPT0;
    } else {
        err = 0;
    }

done:
    if (peerPoint) {
        EC_POINT_free(peerPoint);
    }
    return err;
}
int KeyAgreement::GenerateClientKeyExchange(ByteBuffer* ret) {
    const EC_GROUP* ecGroup = EC_KEY_get0_group(m_OurEcKey);
    const EC_POINT* pubKey = EC_KEY_get0_public_key(m_OurEcKey);

    int length = EC_POINT_point2oct(ecGroup, pubKey, POINT_CONVERSION_UNCOMPRESSED, NULL, NULL, NULL);
    ret->SetLength(length);
    EC_POINT_point2oct(ecGroup, pubKey, POINT_CONVERSION_UNCOMPRESSED, (unsigned char *)(ret->Ptr()), ret->Length(), NULL);
    return 0;
}

int KeyAgreement::GenerateECDHKey(int namedCurve, EC_KEY** key) {
    int nid = 0;
    switch (namedCurve) {
        case NAMED_CURVE_SECP256R1:
            nid = NID_X9_62_prime256v1;
            break;
        case NAMED_CURVE_SECP384R1:
            nid = NID_secp384r1;
            break;
        case NAMED_CURVE_SECP521R1:
            nid = NID_secp521r1;
            break;
        default:
            return -1;
    }

    *key = EC_KEY_new_by_curve_name(nid);
    if (key == NULL) {
        return VTLS_ERR_CRYPT0;
    }

    if (EC_KEY_generate_key(*key) != 1) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}