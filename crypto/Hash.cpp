
#include "Hash.h"

#include <cassert>

Hash::Hash(): m_Ctx(NULL) {
}

Hash::~Hash() {
    if (m_Ctx) {
        EVP_MD_CTX_free(m_Ctx);
    }
    m_Ctx = NULL;
}
int Hash::Init(int alg) {
    m_Ctx = EVP_MD_CTX_new();
    const EVP_MD* md = GetEvpMD(alg);
    if (md == NULL) {
        return VTLS_ERR_CRYPT0;
    }
    if (!EVP_DigestInit_ex(m_Ctx, md, NULL)) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}
int Hash::Update(const char* b, int bLen) {
    int success = EVP_DigestUpdate(m_Ctx, b, bLen);
    if (!success) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}
int Hash::Finish() {
    unsigned int size = EVP_MD_CTX_get_size(m_Ctx);
    m_Result.SetLength(size);
    int success = EVP_DigestFinal(m_Ctx, (unsigned char*)m_Result.Ptr(), &size);
    if (!success) {
        return VTLS_ERR_CRYPT0;
    }
    return 0;
}

int Hash::QuickCalc(int alg, const char* b, int bLen) {
    int err = 0;
    if (err = Init(alg)) return err;
    if (err = Update(b, bLen)) return err;
    if (err = Finish()) return err;
    return 0;
}

int Hash::GetHashSize(int hashAlg) {
    switch (hashAlg) {
        case HASH_ALG_MD4:    return 16;
        case HASH_ALG_MD5:    return 16;
        case HASH_ALG_SHA1:   return 20;
        case HASH_ALG_SHA256: return 32;
    }
    assert(false); // TODO:
    return 0;
}

const EVP_MD* Hash::GetEvpMD(int hashAlg) {
    switch (hashAlg) {
    case HASH_ALG_MD4:       return EVP_md4();
    case HASH_ALG_MD5:       return EVP_md5();
    case HASH_ALG_SHA1:      return EVP_sha1();
    case HASH_ALG_SHA224:    return EVP_sha224();
    case HASH_ALG_SHA256:    return EVP_sha256();
    case HASH_ALG_SHA384:    return EVP_sha384();
    case HASH_ALG_SHA512:    return EVP_sha512();
    case HASH_ALG_SHA3224:   return EVP_sha3_224();
    case HASH_ALG_SHA3256:   return EVP_sha3_256();
    case HASH_ALG_SHA3384:   return EVP_sha3_384();
    case HASH_ALG_SHA3512:   return EVP_sha3_512();
    case HASH_ALG_SHA512224: return EVP_sha512_224();
    case HASH_ALG_SHA512256: return EVP_sha512_256();
    }
    return NULL;
}
