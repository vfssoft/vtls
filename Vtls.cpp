

#include "Vtls.h"


#include "messages/ClientHello.h"
#include "messages/ServerHello.h"
#include "messages/ServerKeyExchange.h"
#include "messages/Certificate.h"
#include "messages/ClientKeyExchange.h"
#include "messages/Finished.h"

#define VTLS_STATE_SEND_CLIENT_HELLO        1
#define VTLS_STATE_WAIT_SERVER_HELLO        2
#define VTLS_STATE_WAIT_CERTIFICATE         3
#define VTLS_STATE_WAIT_SERVER_KEY_EXCHANGE 4
#define VTLS_STATE_WAIT_SERVER_HELLO_DONE   5
#define VTLS_STATE_SEND_CLIENT_KEY_EXCHANGE 6
#define VTLS_STATE_SEND_CHANGE_CIPHER_SPEC  7
#define VTLS_STATE_SEND_FINISHED            8
#define VTLS_STATE_WAIT_CHANGE_CIPHER_SPEC  9
#define VTLS_STATE_WAIT_FINISHED            10
#define VTLS_STATE_HANDSHAKE_DONE           11


Vtls::Vtls() {
    m_IsClient = true;
    m_TlsState = VTLS_STATE_SEND_CLIENT_HELLO;
    m_ExtSupportedGroups.Add(NAMED_CURVE_SECP256R1);
    m_ExtSupportedGroups.Add(NAMED_CURVE_SECP384R1);
    m_ExtSupportedGroups.Add(NAMED_CURVE_SECP521R1);
    m_ExtSignatureAlgorithm.Add(ECDSA_SECP256R1_SHA256);
    m_ExtSignatureAlgorithm.Add(ECDSA_SECP384R1_SHA384);
    m_ExtSignatureAlgorithm.Add(ECDSA_SECP521R1_SHA512);
    m_CipherSuite = NULL;

    m_SendConnState.Reset();
    m_RecvConnState.Reset();
    m_NextSendConnState.Reset();
    m_NextRecvConnState.Reset();

    m_Result = 0;
    m_Error.Reset();
}

int Vtls::Process(const char* dataIn, int* lenDataIn, char* dataOut, int* lenDataOut) {
    int errCode = 0;
    int alertDesc = 0;
    bool recvAlert = false;
    Record r;

    if (*lenDataIn) {
        m_Logger->LogHexDump(dataIn, *lenDataIn, false);
        m_InPacker.WriteBytes(dataIn, *lenDataIn);
    }

    // Process possible incoming Alert
    if (NextRecordAvailable()) {
        switch (NextRecordType()) {
            case CT_ALERT:
                if (!ParseRecord(&r) || !ParserAlert(&r)) {
                    alertDesc = VTLS_ALERT_DESC_DECODE_ERROR;
                } else {
                    recvAlert = true;
                }
                goto done;
        }
    }

    switch (m_TlsState) {
        case VTLS_STATE_SEND_CLIENT_HELLO: {
            m_Logger->LogInfo("Send ClientHello");
            Handshake* hs = NewHandshake();
            BuildClientHello(hs);
            BuildHandshakeRecord(&m_OutPacker, hs);
            m_TlsState = VTLS_STATE_WAIT_SERVER_HELLO;
            break;
        }
        case VTLS_STATE_WAIT_SERVER_HELLO: {
            if (!NextRecordAvailable()) break;
            if (!ParseRecord(&r) || r.ContentType != CT_HANDSHAKE) {
                alertDesc = VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
                goto done;
            }
            if (!ParseHandshake(&r)) {
                alertDesc = VTLS_ALERT_DESC_DECODE_ERROR;
                goto done;
            }
            m_Logger->LogInfo("Receive ServerHello");
            if (errCode = ProcessServerHello()) {
                alertDesc = errCode;
                goto done;
            }

            m_TlsState = VTLS_STATE_WAIT_CERTIFICATE; // TODO: It's not always true, but for now, it's always true
            // break;
        }
        case VTLS_STATE_WAIT_CERTIFICATE: {
            if (!NextRecordAvailable()) break;
            if (!ParseRecord(&r) || r.ContentType != CT_HANDSHAKE) {
                alertDesc = VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
                goto done;
            }
            if (!ParseHandshake(&r)) {
                alertDesc = VTLS_ALERT_DESC_DECODE_ERROR;
                goto done;
            }
            m_Logger->LogInfo("Receive Server Certificate");
            if (errCode = ProcessServerCertificates()) {
                alertDesc = errCode;
                goto done;
            }
            m_TlsState = VTLS_STATE_WAIT_SERVER_KEY_EXCHANGE;
            // break;
        }
        case VTLS_STATE_WAIT_SERVER_KEY_EXCHANGE: {
            if (!NextRecordAvailable()) break;
            if (!ParseRecord(&r) || r.ContentType != CT_HANDSHAKE) {
                alertDesc = VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
                goto done;
            }
            if (!ParseHandshake(&r)) {
                alertDesc = VTLS_ALERT_DESC_DECODE_ERROR;
                goto done;
            }
            m_Logger->LogInfo("Receive ServerKeyExchange");
            if (errCode = ProcessServerKeyExchange()) {
                alertDesc = errCode;
                goto done;
            }
            m_TlsState = VTLS_STATE_WAIT_SERVER_HELLO_DONE;
            // break;
        }
        case VTLS_STATE_WAIT_SERVER_HELLO_DONE: {
            if (!NextRecordAvailable()) break;
            if (!ParseRecord(&r) || r.ContentType != CT_HANDSHAKE) {
                alertDesc = VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
                goto done;
            }
            if (!ParseHandshake(&r)) {
                alertDesc = VTLS_ALERT_DESC_DECODE_ERROR;
                goto done;
            }
            m_Logger->LogInfo("Receive ServerHelloDone");
            if (errCode = ProcessServerHelloDone()) {
                alertDesc = errCode;
                goto done;
            }
            m_TlsState = VTLS_STATE_SEND_CLIENT_KEY_EXCHANGE;
            // break;
        }
        case VTLS_STATE_SEND_CLIENT_KEY_EXCHANGE: {
            m_Logger->LogInfo("Send ClientKeyExchange");
            Handshake* hs = NewHandshake();
            BuildClientKeyExchange(hs);
            BuildHandshakeRecord(&m_OutPacker, hs);
            GenerateKeys();
            m_TlsState = VTLS_STATE_SEND_CHANGE_CIPHER_SPEC;
            //break;
        }
        case VTLS_STATE_SEND_CHANGE_CIPHER_SPEC: {
            m_Logger->LogInfo("Send ChangeCipherSpec");
            BuildChangeCipherSpecRecord(&m_OutPacker);

            m_SendConnState.Set(&m_NextSendConnState);
            m_NextSendConnState.Reset();
            m_SendConnState.Enc = true;

            m_TlsState = VTLS_STATE_SEND_FINISHED;
            //break;
        }
        case VTLS_STATE_SEND_FINISHED: {
            m_Logger->LogInfo("Send Finished");
            Handshake* hs = NewHandshake();
            BuildFinished(hs);
            BuildHandshakeRecord(&m_OutPacker, hs);
            m_TlsState = VTLS_STATE_WAIT_CHANGE_CIPHER_SPEC;
            // break;
        }
        case VTLS_STATE_WAIT_CHANGE_CIPHER_SPEC: {
            if (!NextRecordAvailable()) break;
            if (!ParseRecord(&r) || r.ContentType != CT_CHANGE_CIPHER_SPEC) {
                alertDesc = VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
                goto done;
            }
            m_Logger->LogInfo("Receive ChangeCipherSpec");

            m_RecvConnState.Set(&m_NextRecvConnState);
            m_NextRecvConnState.Reset();
            m_RecvConnState.Enc = true;

            m_TlsState = VTLS_STATE_WAIT_FINISHED;
            // break;
        }
        case VTLS_STATE_WAIT_FINISHED: {
            if (!NextRecordAvailable()) break;
            if (!ParseRecord(&r) || r.ContentType != CT_HANDSHAKE) {
                alertDesc = VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
                goto done;
            }
            if (!ParseHandshake(&r)) {
                alertDesc = VTLS_ALERT_DESC_DECODE_ERROR;
                goto done;
            }
            m_Logger->LogInfo("Receive Finished");
            if ((errCode = ProcessFinished())) {
                alertDesc = errCode;
                goto done;
            }
            m_TlsState = VTLS_STATE_HANDSHAKE_DONE;
            // break;
        }

    }

done:
    if (alertDesc) {
        BuildAlert(&m_OutPacker,  VTLS_ALERT_LEVEL_FATAL, alertDesc);

        m_Result = alertDesc;
        m_Error.SetStr(Alert::AlertDescriptionStr(alertDesc));
    } else {
        if (recvAlert) {
            m_Result = m_InAlert.Description();
            m_Error.SetStr(m_InAlert.DescriptionStr());
        }
    }

    if (m_OutPacker.Available() > 0) {
        int outLen = m_OutPacker.ReadBytes(dataOut, *lenDataOut);
        *lenDataOut = outLen;
        m_Logger->LogHexDump(dataOut, *lenDataOut, true);
    } else {
        *lenDataOut = 0;
    }

    return m_Result;
}

bool Vtls::Readable() {
    return m_OutPacker.Available() > 0;
}
bool Vtls::Writable() {
    return m_Result == 0;
}
bool Vtls::HandshakeDone() {
    return m_TlsState == VTLS_STATE_HANDSHAKE_DONE || m_Result != 0;
}
int Vtls::HandshakeResult() {
    return m_Result;
}
const char* Vtls::HandshakeError() {
    return m_Error.Ptr();
}

int Vtls::ProcessServerHello() {
    // process server hello
    Handshake* hs = LastHandshake();
    if (hs->HandshakeType() != HSMT_SERVER_HELLO) {
        return VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
    }

    ServerHello* serverHello = (ServerHello*) hs->Message();
    if (serverHello->Version != TLS_VERSION_1_2) {
        return VTLS_ALERT_DESC_PROTOCOL_VERSION;
    }
    memcpy(m_ServerRandom, serverHello->Random, 32);
    m_SessionID.Set(serverHello->SessionID.Ptr(), serverHello->SessionID.Length());
    m_CipherSuite = m_CipherSuites.Find(serverHello->CipherSuite);
    if (m_CipherSuite == NULL) {
        return VTLS_ALERT_DESC_HANDSHAKE_FAILURE;
    }

    if (serverHello->CompressionMethod != COMPRESSION_NULL) {
        return VTLS_ALERT_DESC_HANDSHAKE_FAILURE;
    }

    // TODO: extensions

    return 0;
}

int Vtls::ProcessServerCertificates() {
    Handshake* hs = LastHandshake();
    if (hs->HandshakeType() != HSMT_CERTIFICATE) {
        return VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
    }
    int err = 0;
    Certificate* certMsg = (Certificate*) hs->Message();
    Array<ByteBuffer>* certs = certMsg->Certificates();

    if (certs->Size() == 0) {
        return VTLS_ALERT_DESC_NO_CERTIFICATE;
    } else {
        for (int i = 0; i < certs->Size(); i++) {
            ByteBuffer* buf = certs->Get(i);
            Cert* c = m_PeerCerts.Add();

            if (err = c->Load(buf->Ptr(), buf->Length())) {
                return VTLS_ALERT_DESC_BAD_CERTIFICATE;
            }
        }
    }
    return 0;
}

int Vtls::ProcessServerKeyExchange() {
    Handshake* hs = LastHandshake();
    if (hs->HandshakeType() != HSMT_SERVER_KEY_EXCHANGE) {
        return VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
    }
    ServerKeyExchange* serverKeyExchange = (ServerKeyExchange*) hs->Message();

    int err = 0;
    int sigType = 0;
    int sigHash = 0;
    switch (serverKeyExchange->SignatureAlgorithm) {
        case ECDSA_SECP256R1_SHA256:
            sigType = SIGNATURE_ECDSA;
            sigHash = HASH_ALG_SHA256;
            break;
        case ECDSA_SECP384R1_SHA384:
            sigType = SIGNATURE_ECDSA;
            sigHash = HASH_ALG_SHA384;
            break;
        case ECDSA_SECP521R1_SHA512:
            sigType = SIGNATURE_ECDSA;
            sigHash = HASH_ALG_SHA512;
            break;
        default:
            return VTLS_ALERT_DESC_ILLEGAL_PARAMETER;
    }

    // Verify Signature
    Hash hash;
    ByteBuffer dataToSign;
    dataToSign.Append(m_ClientRandom, 32);
    dataToSign.Append(m_ServerRandom, 32);
    dataToSign.Append(serverKeyExchange->ToSign.Ptr(), serverKeyExchange->ToSign.Length());
    hash.QuickCalc(sigHash, dataToSign.Ptr(), dataToSign.Length());

    Cert* cert = m_PeerCerts.Get(0);
    EC_KEY* ecKey = EVP_PKEY_get1_EC_KEY(cert->PublicKey());

    int verifyResult = ECDSA_verify(
            0,
            (const unsigned char *)(hash.Result()->Ptr()), hash.Result()->Length(),
            (const unsigned char *)(serverKeyExchange->Signature.Ptr()), serverKeyExchange->Signature.Length(),
            ecKey
    );
    if (verifyResult == 1) {
        // signature is OK
    } else if (verifyResult == 0) {
        return VTLS_ALERT_DESC_HANDSHAKE_FAILURE;
    } else if (verifyResult == -1) {
        return VTLS_ALERT_DESC_INTERNAL_ERROR;
    }

    // Key exchange
    if (err = m_KeyAgreement.ProcessServerKeyExchange(serverKeyExchange->NamedCurve, serverKeyExchange->PubKey.Ptr(), serverKeyExchange->PubKey.Length())) {
        return VTLS_ALERT_DESC_HANDSHAKE_FAILURE;
    }
    return 0;
}
int Vtls::ProcessServerHelloDone() {
    return 0;
}
int Vtls::ProcessFinished() {
    Handshake* hs = LastHandshake();
    if (hs->HandshakeType() != HSMT_FINISHED) {
        return VTLS_ALERT_DESC_UNEXPECTED_MESSAGE;
    }

    Finished* finished = (Finished*) hs->Message();
    ByteBuffer verifyData;
    CalVerifyData(!m_IsClient, &verifyData);

    if (!verifyData.Equals(&finished->VerifyData)) {
        return VTLS_ALERT_DESC_HANDSHAKE_FAILURE;
    }
    return 0;
}

void Vtls::BuildHandshakeRecord(Packer* packer, Handshake* hs) {
    Packer hsPacker;
    hs->Encode(&hsPacker);
    BuildRecord(packer, CT_HANDSHAKE, hsPacker.Ptr(), hsPacker.Available());
}

void Vtls::BuildChangeCipherSpecRecord(Packer* packer) {
    char data[1] = { 0x01 };
    BuildRecord(packer, CT_CHANGE_CIPHER_SPEC, data, 1);
}

void Vtls::BuildRecord(Packer* packer, int ct, const char* fragment, int lenFragment) {
    if (m_SendConnState.Enc) {
        Record plain, enc;
        plain.ContentType = ct;
        plain.ProtocolVersion = TLS_VERSION_1_2;
        plain.Fragment.Set(fragment, lenFragment);

        Encrypt(&plain, &enc);

        enc.Encode(packer);
    } else {
        packer->WriteUInt8(ct);
        packer->WriteUInt16(TLS_VERSION_1_2);
        packer->WriteUInt16(lenFragment);
        packer->WriteBytes(fragment, lenFragment);
    }
}
void Vtls::BuildAlert(Packer* packer, int level, int desc) {
    char alertBytes[] = { (char)level, (char)desc };
    BuildRecord(packer, CT_ALERT, alertBytes, 2);
}

void Vtls::BuildClientHello(Handshake* hs) {
    ClientHello* clientHello = (ClientHello*) Handshake::Build(hs, HSMT_CLIENT_HELLO);
    Crypto::RandomBytes(clientHello->Random, 32);
    memcpy(m_ClientRandom, clientHello->Random, 32);
    for (int i = 0; i < m_CipherSuites.Ciphers.Size(); i++) {
        clientHello->CipherSuites.Add(m_CipherSuites.Ciphers.Get(i)->ID);
    }
    for (int i = 0; i < m_ExtSupportedGroups.Size(); i++) {
        clientHello->ExtSupportedGroups.Add(m_ExtSupportedGroups.Get(i));
    }
    for (int i = 0; i < m_ExtSignatureAlgorithm.Size(); i++) {
        clientHello->ExtSignatureAlgorithms.Add(m_ExtSignatureAlgorithm.Get(i));
    }
}
void Vtls::BuildClientKeyExchange(Handshake* hs) {
    ClientKeyExchange* clientKeyExchange = (ClientKeyExchange*) Handshake::Build(hs, HSMT_CLIENT_KEY_EXCHANGE);
    m_KeyAgreement.GenerateClientKeyExchange(&clientKeyExchange->PubKey);
}
void Vtls::BuildFinished(Handshake* hs) {
    Finished* finished = (Finished*) Handshake::Build(hs, HSMT_FINISHED);
    CalVerifyData(m_IsClient, &finished->VerifyData);
}


void Vtls::GenerateKeys() {
    PRF prf;

    ByteBuffer* preMasterKey = m_KeyAgreement.SharedKey();
    m_Logger->LogVerb("PreMasterSecret: %s", ToHex(preMasterKey->Ptr(),preMasterKey->Length()));

    prf.GenMasterFromPreMaster(
            m_CipherSuite,
            preMasterKey->Ptr(), preMasterKey->Length(),
            m_ClientRandom, 32,
            m_ServerRandom, 32,
            &m_MasterKey
    );
    m_Logger->LogVerb("ClientRandom: %s", ToHex(m_ClientRandom, 32));
    m_Logger->LogVerb("ServerRandom: %s", ToHex(m_ServerRandom, 32));
    m_Logger->LogVerb("MasterSecret: %s", ToHex(m_MasterKey.Ptr(),m_MasterKey.Length()));

    ByteBuffer clientMac, serverMac, clientKey, serverKey, clientIV, serverIV;
    prf.GenKeys(
            m_CipherSuite,
            m_MasterKey.Ptr(), m_MasterKey.Length(),
            m_ClientRandom, 32, m_ServerRandom, 32,
            m_CipherSuite->MacLen, m_CipherSuite->KeyLen, m_CipherSuite->IVLen,
            &clientMac, &serverMac, &clientKey, &serverKey, &clientIV, &serverIV
    );
    m_Logger->LogVerb("ClientMac: %s", ToHex(clientMac.Ptr(), clientMac.Length()));
    m_Logger->LogVerb("ServerMac: %s", ToHex(serverMac.Ptr(), serverMac.Length()));
    m_Logger->LogVerb("ClientKey: %s", ToHex(clientKey.Ptr(), clientKey.Length()));
    m_Logger->LogVerb("ServerKey: %s", ToHex(serverKey.Ptr(), serverKey.Length()));
    m_Logger->LogVerb("ClientIV: %s", ToHex(clientIV.Ptr(), clientIV.Length()));
    m_Logger->LogVerb("ServerIV: %s", ToHex(serverIV.Ptr(), serverIV.Length()));

    if (m_IsClient) {
        m_NextSendConnState.Mac.Set(&clientMac);
        m_NextSendConnState.Key.Set(&clientKey);
        m_NextSendConnState.IV.Set(&clientIV);
        m_NextRecvConnState.Mac.Set(&serverMac);
        m_NextRecvConnState.Key.Set(&serverKey);
        m_NextRecvConnState.IV.Set(&serverIV);
    } else {
        m_NextSendConnState.Mac.Set(&serverMac);
        m_NextSendConnState.Key.Set(&serverKey);
        m_NextSendConnState.IV.Set(&serverIV);
        m_NextRecvConnState.Mac.Set(&clientMac);
        m_NextRecvConnState.Key.Set(&clientKey);
        m_NextRecvConnState.IV.Set(&clientIV);
    }
}
void Vtls::CalVerifyData(bool client, ByteBuffer* result) {
    PRF prf;
    ByteBuffer handshakeMessageBytes;
    HandshakeMessagesBytes(true, &handshakeMessageBytes);
    const char* label = client ? "client finished" : "server finished";
    prf.GenVerifyData(m_CipherSuite, &handshakeMessageBytes, m_MasterKey.Ptr(), m_MasterKey.Length(), label, result);
}
void Vtls::HandshakeMessagesBytes(bool forFinished, ByteBuffer* result) {
    int count = m_HandshakeMessages.Size();
    if (forFinished) {
        if (m_HandshakeMessages.Get(count-1)->HandshakeType() == HSMT_FINISHED) {
            count--;
        }
    }
    for (int i = 0; i < count; i++) {
        result->Append(m_HandshakeMessages.Get(i)->RawData());
    }
}

bool Vtls::Encrypt(Record* plain, Record* enc) {
    AES aesGcm;
    Packer nonce, aad, encFrag;

    nonce.WriteBytes(m_SendConnState.IV.Ptr(), m_SendConnState.IV.Length());
    nonce.WriteUInt64(m_SendConnState.SeqNo);

    // additional_data = seq_num + TLSCompressed.type + TLSCompressed.version + TLSCompressed.length;
    aad.WriteUInt64(m_SendConnState.SeqNo);
    aad.WriteUInt8(plain->ContentType);
    aad.WriteUInt16(plain->ProtocolVersion);
    aad.WriteUInt16(plain->Fragment.Length());

    m_Logger->LogVerb("Encrypt Record: Sequence Number=%d", m_SendConnState.SeqNo);
    m_Logger->LogVerb("Encrypt Record: Nonce=%s", ToHex(nonce.Ptr(), nonce.Available()));
    m_Logger->LogVerb("Encrypt Record: AAD=%s", ToHex(aad.Ptr(), aad.Available()));
    m_Logger->LogVerb("Encrypt Record: Plaintext=%s", ToHex(plain->Fragment.Ptr(), plain->Fragment.Length()));

    int aesAlg = m_SendConnState.Key.Length() == 16 ? MY_AES_128_GCM : MY_AES_256_CCM;
    aesGcm.Init(aesAlg, m_SendConnState.Key.Ptr(), nonce.Ptr(), true);
    aesGcm.SetGcmIVLen(nonce.Available());
    aesGcm.SetAAD(aad.Ptr(), aad.Available());
    aesGcm.Update(plain->Fragment.Ptr(), plain->Fragment.Length());
    aesGcm.Final();

    ByteBuffer* tag = aesGcm.GcmTag();
    encFrag.WriteUInt64(m_SendConnState.SeqNo);
    encFrag.WriteBytes(aesGcm.Result()->Ptr(), aesGcm.Result()->Length());
    encFrag.WriteBytes(tag->Ptr(), tag->Length());

    m_Logger->LogVerb("Encrypted Record: Ciphertext=%s", ToHex(aesGcm.Result()->Ptr(), aesGcm.Result()->Length()));
    m_Logger->LogVerb("Encrypted Record: Tag=%s", ToHex(tag->Ptr(), tag->Length()));

    enc->ContentType = plain->ContentType;
    enc->ProtocolVersion = plain->ProtocolVersion;
    enc->Fragment.Set(encFrag.Ptr(), encFrag.Available());

    m_SendConnState.SeqNo++;
    return true;
}
bool Vtls::Decrypt(Record* enc, Record* plain) {
    int err = 0;
    AES aesGcm;
    Packer nonce, aad;
    ByteBuffer explicitNonce, ciphertext, tag;

    explicitNonce.Set(enc->Fragment.Ptr(), 8); // sequence number
    ciphertext.Set(enc->Fragment.Ptr() + 8, enc->Fragment.Length() - 8 - 16);
    tag.Set(enc->Fragment.Ptr() + enc->Fragment.Length() - 16, 16);

    nonce.WriteBytes(m_RecvConnState.IV.Ptr(), m_RecvConnState.IV.Length());
    nonce.WriteBytes(explicitNonce.Ptr(), explicitNonce.Length());

    aad.WriteUInt64(m_RecvConnState.SeqNo);
    aad.WriteUInt8(enc->ContentType);
    aad.WriteUInt16(enc->ProtocolVersion);
    aad.WriteUInt16(ciphertext.Length());

    m_Logger->LogVerb("Decrypt Record: Sequence Number=%d", m_RecvConnState.SeqNo);
    m_Logger->LogVerb("Decrypt Record: Nonce=%s", ToHex(nonce.Ptr(), nonce.Available()));
    m_Logger->LogVerb("Decrypt Record: AAD=%s", ToHex(aad.Ptr(), aad.Available()));
    m_Logger->LogVerb("Decrypt Record: Ciphertext=%s", ToHex(ciphertext.Ptr(), ciphertext.Length()));
    m_Logger->LogVerb("Decrypt Record: Tag=%s", ToHex(tag.Ptr(), tag.Length()));

    int aesAlg = m_RecvConnState.Key.Length() == 16 ? MY_AES_128_GCM : MY_AES_256_CCM;
    if ((err = aesGcm.Init(aesAlg, m_RecvConnState.Key.Ptr(), nonce.Ptr(), false))) return false;
    if ((err = aesGcm.SetGcmIVLen(nonce.Available()))) return false;
    if ((err = aesGcm.SetAAD(aad.Ptr(), aad.Available()))) return false;
    if ((err = aesGcm.SetGcmTag(tag.Ptr(), tag.Length()))) return false;
    if ((err = aesGcm.Update(ciphertext.Ptr(), ciphertext.Length()))) return false;
    if ((err = aesGcm.Final())) return false;

    plain->ContentType = enc->ContentType;
    plain->ProtocolVersion = enc->ProtocolVersion;
    plain->Fragment.Set(aesGcm.Result());

    m_Logger->LogVerb("Decrypted Record: Plain=%s", ToHex(plain->Fragment.Ptr(), plain->Fragment.Length()));

    m_RecvConnState.SeqNo++;
    return true;
}

bool Vtls::NextRecordAvailable() {
    if (m_InPacker.Available() < 5)
        return false;

    int rPos = m_InPacker.ReadPosition();
    int len = (m_InPacker.Ptr()[rPos + 3] << 8) +  m_InPacker.Ptr()[rPos + 4];
    return m_InPacker.Available() >= len + 5;
}
int Vtls::NextRecordType() {
    int rPos = m_InPacker.ReadPosition();
    return m_InPacker.Available() == 0 ? 0 : m_InPacker.Ptr()[rPos];
}
bool Vtls::ParseRecord(Record* r) {
    if (m_RecvConnState.Enc) {
        Record encRecord;
        if (!encRecord.Decode(&m_InPacker)) return false;
        if (!Decrypt(&encRecord, r)) return false;
        return true;
    } else {
        return r->Decode(&m_InPacker);
    }
}
bool Vtls::ParserAlert(Record* r) {
    Packer p(&r->Fragment);
    int level = p.ReadUInt8();
    int desc  = p.ReadUInt8();
    m_InAlert.Set(level, desc);
    return true;
}
bool Vtls::ParseHandshake(Record* r) {
    Packer p(&r->Fragment);
    Handshake* hs = NewHandshake();
    hs->Decode(&p);
    return true;
}
Handshake* Vtls::NewHandshake() {
    return m_HandshakeMessages.Add();
}
Handshake* Vtls::LastHandshake() {
    return m_HandshakeMessages.Get(m_HandshakeMessages.Size() - 1);
}



const char* Vtls::ToHex(const char* data, int len) {
    m_HexBuf.SetHex(data, len);
    return m_HexBuf.Ptr();
}