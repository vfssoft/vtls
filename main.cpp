
#include <cstdlib>

#include "Socket.h"
#include "Vtls.h"
#include "utils/Logger.h"

int main() {
    char* dataIn = new char[4096];
    int lenDataIn = 0;
    char* dataOut = new char[4096];
    int lenDataOut = 4096;

    int err = 0;
    Logger logger;
    Vtls tls;
    SelectInfo selectInfo;
    Socket client(0);

    logger.SetLogLevel(VTLS_LOG_VERB);
    tls.SetLogger(&logger);

    client.SetRemoteHost("localhost");
    client.SetRemotePort(9999);
    if (err = client.Connect()) return err;

    while (true) {
        selectInfo.Set(20);

        if (err = client.Select(&selectInfo)) {
            return err;
        }

        if (selectInfo.Timeout()) {
            continue;
        }
        if (selectInfo.Writable()) {
            lenDataIn = 0;
            lenDataOut = 4096;
            if (err = tls.Process(NULL, &lenDataIn, dataOut, &lenDataOut)) {
                return err;
            }
            if (lenDataOut) {
                if (err = client.Send(dataOut, &lenDataOut)) {
                    return err;
                }
            }
        }
        if (selectInfo.Readable()){
            lenDataIn = 4096;
            if (err = client.Recv(dataIn, &lenDataIn)) {
                return err;
            }
            if (lenDataIn) {
                lenDataOut = 4096;
                if (err = tls.Process(dataIn, &lenDataIn, dataOut, &lenDataOut)) {
                    return err;
                }
                if (lenDataOut) {
                    if (err = client.Send(dataOut, &lenDataOut)) {
                        return err;
                    }
                }
            }
        }
    }

    return 0;
}