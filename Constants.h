

#ifndef VTLS_CONSTANTS_H
#define VTLS_CONSTANTS_H

// TLS Version
#define TLS_VERSION_1_2 0x0303

// TLS content types.
#define CT_CHANGE_CIPHER_SPEC 20
#define CT_ALERT              21
#define CT_HANDSHAKE          22
#define CT_APP_DATA           23


// TLS handshake message types
#define HSMT_HELLO_REUQEST        0
#define HSMT_CLIENT_HELLO         1
#define HSMT_SERVER_HELLO         2
#define HSMT_NEW_SESSION_TICKET   4
#define HSMT_END_OF_EARLY_DATA    5
#define HSMT_ENCRYPTED_EXTENSIONS 8
#define HSMT_CERTIFICATE          11
#define HSMT_SERVER_KEY_EXCHANGE  12
#define HSMT_CERTIFICATE_REQUEST  13
#define HSMT_SERVER_HELLO_DONE    14
#define HSMT_CERTIFICATE_VERIFY   15
#define HSMT_CLIENT_KEY_EXCHANGE  16
#define HSMT_FINISHED             20
#define HSMT_CERTIFICATE_STATUS   22
#define HSMT_KEY_UPDATE           24

// TLS Compression types
#define COMPRESSION_NULL          0

// Cipher Suites (TLS 1.2)
#define TLS_RSA_WITH_RC4_128_SHA                      0x0005
#define TLS_RSA_WITH_3DES_EDE_CBC_SHA                 0x000A
#define TLS_RSA_WITH_AES_128_CBC_SHA                  0x002F
#define TLS_RSA_WITH_AES_256_CBC_SHA                  0x0035
#define TLS_RSA_WITH_AES_128_CBC_SHA256               0x003C
#define TLS_RSA_WITH_AES_128_GCM_SHA256               0x009C
#define TLS_RSA_WITH_AES_256_GCM_SHA384               0x009D

#define TLS_ECDHE_ECDSA_WITH_RC4_128_SHA              0xC007
#define TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA          0xC009
#define TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA          0xC00a
#define TLS_ECDHE_RSA_WITH_RC4_128_SHA                0xC011
#define TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA1          0xC012
#define TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA            0xC013
#define TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA            0xC014
#define TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256       0xC023
#define TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256         0xC027
#define TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256         0xC02F
#define TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256       0xC02B
#define TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384         0xC030
#define TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384       0xC02C
#define TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256   0xCCA8
#define TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 0xCCA9

// Cipher Suites (TLS 1.3)
#define TLS_AES_128_GCM_SHA256                        0x1301
#define TLS_AES_256_GCM_SHA384                        0x1302
#define TLS_CHACHA20_POLY1305_SHA256                  0x1303

#define TLS_FALLBACK_SCSV                             0x5600



// Extension Types
#define EXT_TYPE_SUPPORTED_GROUPS      10
#define EXT_TYPE_EC_POINT_FORMATS      11
#define EXT_TYPE_SIGNATURE_ALGORITHM   13

// Signature algorithms
#define ECDSA_SECP256R1_SHA256         0x0403
#define ECDSA_SECP384R1_SHA384         0x0503
#define ECDSA_SECP521R1_SHA512         0x0603

// Namned Curve
#define NAMED_CURVE_SECP256R1 23
#define NAMED_CURVE_SECP384R1 24
#define NAMED_CURVE_SECP521R1 25
#define NAMED_CURVE_X25519    29



#define SIGNATURE_PKCS1_V15            225
#define SIGNATURE_RSA_PSS              226
#define SIGNATURE_ECDSA                227
#define SIGNATURE_ED25519              228

// Hash algorithm id
#define HASH_ALG_MD4       0
#define HASH_ALG_MD5       1
#define HASH_ALG_SHA1      2
#define HASH_ALG_SHA224    3
#define HASH_ALG_SHA256    4
#define HASH_ALG_SHA384    5
#define HASH_ALG_SHA512    6
#define HASH_ALG_SHA3224   7
#define HASH_ALG_SHA3256   8
#define HASH_ALG_SHA3384   9
#define HASH_ALG_SHA3512   10
#define HASH_ALG_SHA512224 11
#define HASH_ALG_SHA512256 12

#endif //VTLS_CONSTANTS_H
