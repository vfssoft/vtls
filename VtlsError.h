
#ifndef VTLS_VTLSERROR_H
#define VTLS_VTLSERROR_H

#define VTLS_ERR_SUCCESS                0
#define VTLS_ERR_RESOLVE_ADDR           0xF0000001
#define VTLS_ERR_SOCK_DISC_GRACEFULLY   0xF0000002
#define VTLS_ERR_SOCK                   0xF0000003
#define VTLS_ERR_ENCODING               0xF0000004
#define VTLS_ERR_ALERT                  0xF0000005
#define VTLS_ERR_CRYPT0                 0xF0000006


static char g_ErrorMsg[1024];


#endif //VTLS_VTLSERROR_H
