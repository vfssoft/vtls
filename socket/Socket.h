
#ifndef VTLS_SOCKET_H
#define VTLS_SOCKET_H

#include <winsock2.h>
#include <ws2tcpip.h>

#include "../VtlsError.h"
#include "../utils/ByteBuffer.h"

#define VTLS_SOCKET_ROLE_CLIENT      0
#define VTLS_SOCKET_ROLE_ACCEPTOR    1
#define VTLS_SOCKET_ROLE_SERVER      2

#define VTLS_SOCKET_HOW_RECEIVE      0
#define VTLS_SOCKET_HOW_SEND         1
#define VTLS_SOCKET_HOW_BOTH         2

class SelectInfo {
public:
    SelectInfo() : m_Readable(false), m_Writable(false), m_TimeoutMS(20), m_Timedout(false) {}

    void Set(int timeoutMS) {
        m_TimeoutMS = timeoutMS;

        m_Readable = false;
        m_Writable = false;
        m_Timedout = false;
    }

    bool Readable() const { return m_Readable; }
    bool Writable() const { return m_Writable; }
    bool Timeout()  const { return m_Timedout; }

private:
    int  m_TimeoutMS;

    bool m_Readable;
    bool m_Writable;
    bool m_Timedout;

    friend class Socket;
};

class Socket {
public:
    Socket(int role);

    const char* RemoteHost() { return m_RemoteHost.Ptr(); }
    void SetRemoteHost(const char* val) { m_RemoteHost.SetStr(val); }
    int RemotePort() { return m_RemotePort; }
    void SetRemotePort(int val) { m_RemotePort = val; }

    int Listen();
    int Accept(Socket* newSocket);

    int Connect();
    int Send(const char* buf, int* len);
    int Recv(char* buf, int* len);

    int Select(SelectInfo* selectInfo);
    int Shutdown(int how=VTLS_SOCKET_HOW_BOTH);

private:
    void WsaStartup();
    int ResolveAddr(const char* host, unsigned long* addr);

private:
    SOCKET     m_Socket;
    int        m_Role;
    ByteBuffer m_LocalHost;
    int        m_LocalPort;
    ByteBuffer m_RemoteHost;
    int        m_RemotePort;


};


#endif //VTLS_SOCKET_H
