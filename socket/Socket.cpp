
#include "Socket.h"

Socket::Socket(int role) {
    WsaStartup();
    m_Socket = INVALID_SOCKET;
    m_LocalPort = 0;
    m_RemotePort = 0;
}


int Socket::Listen() {
    int errCode = 0;
    unsigned long lip = 0;
    sockaddr_in lAddr = { 0 };
    memset(&lAddr, 0, sizeof(sockaddr_in));

    m_Role = VTLS_SOCKET_ROLE_ACCEPTOR;

    errCode = ResolveAddr(m_LocalHost.Ptr(), &lip);
    if (errCode) return errCode;

    m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (m_Socket == INVALID_SOCKET) {
        return VTLS_ERR_SOCK;
    }

    lAddr.sin_family      = AF_INET;
    lAddr.sin_addr.s_addr = lip;
    lAddr.sin_port        = htons(m_LocalPort);

    errCode = bind(m_Socket, (sockaddr*)&lAddr, sizeof(sockaddr));
    if (errCode) {
        goto done;
    }

    errCode = listen(m_Socket, SOMAXCONN);
    if (errCode) {
        goto done;
    }

done:
    if (errCode == 0) {
        return 0;
    } else {
        if (m_Socket != INVALID_SOCKET) {
            closesocket(m_Socket);
            m_Socket = INVALID_SOCKET;
        }
        return VTLS_ERR_SOCK;
    }
}

int Socket::Accept(Socket* newSocket) {
    SOCKADDR_IN addr = { 0 };
    int addrSize = sizeof(SOCKADDR_IN);

    SOCKET s = accept(m_Socket, NULL, NULL);
    if (s == INVALID_SOCKET) {
        return VTLS_ERR_SOCK;
    }

    newSocket->m_Socket = s;

    getpeername(m_Socket, (struct sockaddr*)&addr, &addrSize);
    newSocket->m_RemoteHost.SetStr(inet_ntoa(addr.sin_addr));
    newSocket->m_RemotePort = ntohl(addr.sin_port);

    getsockname(m_Socket, (struct sockaddr*)&addr, &addrSize);
    newSocket->m_LocalHost.SetStr(inet_ntoa(addr.sin_addr));
    newSocket->m_LocalPort = ntohl(addr.sin_port);

    return 0;
}

int Socket::Connect() {
    int errCode = 0;
    unsigned long rip = 0;
    unsigned long lip = 0;
    sockaddr_in rAddr = { 0 };
    sockaddr_in lAddr = { 0 };

    m_Role = VTLS_SOCKET_ROLE_CLIENT;

    memset(&rAddr, 0, sizeof(sockaddr_in));
    memset(&lAddr, 0, sizeof(sockaddr_in));

    errCode = ResolveAddr(m_RemoteHost.Ptr(), &rip);
    if (errCode) return errCode;

    errCode = ResolveAddr(m_LocalHost.Ptr(), &lip);
    if (errCode) return errCode;

    m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (m_Socket == INVALID_SOCKET) {
        return VTLS_ERR_SOCK;
    }

    rAddr.sin_family      = AF_INET;
    rAddr.sin_addr.s_addr = rip;
    rAddr.sin_port        = htons(m_RemotePort);

    if (m_LocalHost.Length() > 0 || m_LocalPort > 0) {
        lAddr.sin_family      = AF_INET;
        lAddr.sin_addr.s_addr = lip;
        lAddr.sin_port        = htons(m_LocalPort);

        errCode = bind(m_Socket, (sockaddr*)&lAddr, sizeof(sockaddr));
        if (errCode) {
            goto done;
        }
    }

    errCode = connect(m_Socket, (sockaddr*)&rAddr, sizeof(sockaddr));
    if (errCode) {
        goto done;
    }

done:
    if (errCode == 0) {
        return 0;
    } else {
        if (m_Socket != INVALID_SOCKET) {
            closesocket(m_Socket);
            m_Socket = INVALID_SOCKET;
        }
        return VTLS_ERR_SOCK;
    }
}

int Socket::Send(const char* buf, int* len) {
    int errCode = send(m_Socket, buf, *len, 0);
    if (errCode > 0) {
        *len = errCode;
        return 0;
    } else {
        return VTLS_ERR_SOCK;
    }
}

int Socket::Recv(char* buf, int* len) {
    int errCode = recv(m_Socket, buf, *len, 0);
    if (errCode > 0) {
        *len = errCode;
        return 0;
    } else if (errCode == 0) {
        return VTLS_ERR_SOCK_DISC_GRACEFULLY;
    } else {
        return VTLS_ERR_SOCK;
    }
}

int Socket::Select(SelectInfo* selectInfo) {

    fd_set readfds;
    fd_set writefds;
    fd_set exceptfds;
    timeval timeout = { 0  };

    timeout.tv_sec = selectInfo->m_TimeoutMS / 1000;
    timeout.tv_usec = (selectInfo->m_TimeoutMS - timeout.tv_sec * 1000) * 1000;

    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);

    FD_SET(m_Socket, &readfds);
    FD_SET(m_Socket, &writefds);
    FD_SET(m_Socket, &exceptfds);

    int res = select(0, &readfds, &writefds, &exceptfds, &timeout);
    if (res == SOCKET_ERROR) {
        // WSAGetLastError();
        return VTLS_ERR_SOCK;
    } else if (res == 0) {
        selectInfo->m_Timedout = true;
        return 0;
    }

    if (FD_ISSET(m_Socket, &readfds)) {
        // readfds:
        // - If listen has been called and a connection is pending, accept will succeed.
        // - Data is available for reading (includes OOB data if SO_OOBINLINE is enabled).
        // - Connection has been closed/reset/terminated.
        selectInfo->m_Readable = true;
    }
    if (FD_ISSET(m_Socket, &writefds)) {
        // writefds:
        // - If processing a connect call (nonblocking), connection has succeeded.
        // - Data can be sent.
        selectInfo->m_Writable = true;
    }
    if (FD_ISSET(m_Socket, &exceptfds)) {
        // exceptfds:
        // - If processing a connect call (nonblocking), connection attempt failed.
        // - OOB data is available for reading (only if SO_OOBINLINE is disabled).

        // ignore it, we don't use OOB or nonblocking connect.
    }

    return 0;
}


int Socket::Shutdown(int how) {
    shutdown(m_Socket, how);
    return 0;
}


void Socket::WsaStartup() {
    WSADATA wsaData;
    int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    //assert(result == NO_ERROR);
}


int Socket::ResolveAddr(const char* host, unsigned long* addr) {
    if (host == NULL || host[0] == 0) {
        *addr = INADDR_ANY;
        return 0;
    }

    *addr = inet_addr(host);
    if (*addr != INADDR_NONE) {
       return 0;
    }

    struct addrinfo* result = NULL;
    struct addrinfo* ptr    = NULL;

    struct addrinfo hints = { 0 };
    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    int errCode = getaddrinfo(host, NULL, &hints, &result);
    if (errCode != 0) {
        return VTLS_ERR_RESOLVE_ADDR;
    }

    for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        if (ptr->ai_family == AF_INET) {
            struct sockaddr_in *addrin = (struct sockaddr_in*)ptr->ai_addr;
            *addr = addrin->sin_addr.s_addr;
            break;
        }
    }
    freeaddrinfo(result);

    if (ptr == NULL) {
        return VTLS_ERR_RESOLVE_ADDR;
    }
    return 0;
}