
#ifndef VTLS_VTLS_H
#define VTLS_VTLS_H

#include <cstdlib>
#include <cstring>

#include "utils/Packer.h"
#include "utils/ValArr.h"
#include "utils/Array.h"
#include "utils/Logger.h"
#include "crypto/Crypto.h"
#include "crypto/Hash.h"
#include "crypto/AES.h"
#include "crypto/Cert.h"
#include "crypto/KeyAgreement.h"
#include "crypto/CipherSuite.h"
#include "crypto/PRF.h"
#include "crypto/ConnState.h"
#include "VtlsError.h"

#include "messages/Record.h"
#include "messages/Handshake.h"
#include "messages/Alert.h"

class Vtls {
public:
    Vtls();

    bool IsClient() const { return m_IsClient; }
    void SetIsClient(bool val) { m_IsClient = true; }
    Logger* GetLogger() { return m_Logger; }
    void SetLogger(Logger* logger) { m_Logger = logger; }
    ValArr<UINT16>* ExtSupportedGroups() { return &m_ExtSupportedGroups; }

    int Process(const char* dataIn, int* lenDataIn, char* dataOut, int* lenDataOut);

    // state query
    bool Readable();
    bool Writable();
    bool HandshakeDone();
    int  HandshakeResult();
    const char* HandshakeError();

private:
    int ProcessServerHello();
    int ProcessServerCertificates();
    int ProcessServerKeyExchange();
    int ProcessServerHelloDone();
    int ProcessFinished();

    void BuildClientHello(Handshake* hs);
    void BuildClientKeyExchange(Handshake* hs);
    void BuildFinished(Handshake* hs);
    void BuildHandshakeRecord(Packer* packer, Handshake* hs);
    void BuildChangeCipherSpecRecord(Packer* packer);
    void BuildRecord(Packer* packer, int ct, const char* fragment, int lenFragment);
    void BuildAlert(Packer* packer, int level, int desc);

    void GenerateKeys();
    void CalVerifyData(bool client, ByteBuffer* result);
    void HandshakeMessagesBytes(bool forFinished, ByteBuffer* result);
    bool Encrypt(Record* plain, Record* enc);
    bool Decrypt(Record* enc, Record* plain);

    bool NextRecordAvailable();
    int  NextRecordType();
    bool ParseRecord(Record* r);
    bool ParserAlert(Record* r);
    bool ParseHandshake(Record* r);
    Handshake* NewHandshake();
    Handshake* LastHandshake();

    const char* ToHex(const char* data, int len);
private:
    bool           m_IsClient;
    CipherSuites   m_CipherSuites;
    ValArr<UINT16> m_ExtSupportedGroups;
    ValArr<UINT16> m_ExtSignatureAlgorithm;
    Logger*        m_Logger;

    char           m_ClientRandom[32];
    char           m_ServerRandom[32];
    ByteBuffer     m_SessionID;
    CipherSuite*   m_CipherSuite;
    Array<Cert>    m_PeerCerts;
    KeyAgreement   m_KeyAgreement;
    Array<Handshake> m_HandshakeMessages;
    ByteBuffer       m_MasterKey;
    ConnState        m_SendConnState;
    ConnState        m_RecvConnState;
    ConnState        m_NextSendConnState;
    ConnState        m_NextRecvConnState;

    int    m_TlsState;
    Packer m_InPacker;
    Packer m_OutPacker;

    int        m_Result;
    ByteBuffer m_Error;

    Alert     m_InAlert;

    // Utils
    ByteBuffer m_HexBuf;
};


#endif //VTLS_VTLS_H
