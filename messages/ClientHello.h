

#ifndef VTLS_CLIENTHELLO_H
#define VTLS_CLIENTHELLO_H

#include "HsMessage.h"
#include "../utils/ValArr.h"

class ClientHello : public HSMessage {
public:
    int             Version;
    char            Random[32];
    ByteBuffer      SessionID; // 0 -> 32
    ValArr<UINT16>  CipherSuites;
    ValArr<UINT8>   CompressionMethods;
    ValArr<UINT16>  ExtSupportedGroups;
    ValArr<UINT16>  ExtSignatureAlgorithms;

    ClientHello();

    bool Encode(Packer* p);
    bool Decode(Packer* p, int len);
};


#endif //VTLS_CLIENTHELLO_H
