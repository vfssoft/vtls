#include "ServerHello.h"



ServerHello::ServerHello(): Version(0), CompressionMethod(COMPRESSION_NULL) {
    Crypto::RandomBytes(Random, 32);
}

bool ServerHello::Encode(Packer *p) {
    p->WriteUInt16(Version);
    p->WriteBytes(Random, 32);

    p->WriteUInt8(SessionID.Length());
    p->WriteBytes(SessionID.Ptr(), SessionID.Length());

    p->WriteUInt16(CipherSuite);
    p->WriteUInt8(CompressionMethod);

    return true;
}

bool ServerHello::Decode(Packer *p, int len) {
    int pktLen = len;
    int startPos = p->ReadPosition();

    Version = p->ReadUInt16();
    p->ReadBytes(Random, 32);

    len = p->ReadUInt8();
    SessionID.SetLength(len);
    if (len > 0) {
        p->ReadBytes(SessionID.Ptr(), len);
    }

    CipherSuite = p->ReadUInt16();
    CompressionMethod = p->ReadUInt8();

    if (p->ReadPosition() - startPos < pktLen) {
        // Extensions
        int extsLen = p->ReadUInt16();
        char ignored[4096];
        p->ReadBytes(ignored, extsLen);
    }

    return true;
}