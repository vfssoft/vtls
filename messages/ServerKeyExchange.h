
#ifndef VTLS_SERVERKEYEXCHANGE_H
#define VTLS_SERVERKEYEXCHANGE_H

#include "HsMessage.h"

#include "../utils/Array.h"
#include "../utils/ByteBuffer.h"

class ServerKeyExchange : public HSMessage {
public:
    ServerKeyExchange();

    bool Encode(Packer *p) override;
    bool Decode(Packer *p, int len) override;

    //UINT8      CurveType; // only named curve is supported now
    UINT16     NamedCurve;
    ByteBuffer PubKey;

    ByteBuffer ToSign;
    UINT16     SignatureAlgorithm;
    ByteBuffer Signature;
};


#endif //VTLS_SERVERKEYEXCHANGE_H
