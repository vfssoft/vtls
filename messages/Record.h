

#ifndef VTLS_RECORD_H
#define VTLS_RECORD_H

#include "../utils/ByteBuffer.h"
#include "../utils/Packer.h"

class Record {
public:
    int        ContentType;
    int        ProtocolVersion;
    ByteBuffer Fragment;

    bool Encode(Packer* p);
    bool Decode(Packer* p);

    ByteBuffer* RawData() { return &m_RawData; }

private:
    ByteBuffer m_RawData;
};


#endif //VTLS_RECORD_H
