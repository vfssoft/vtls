

#ifndef VTLS_SERVERHELLODONE_H
#define VTLS_SERVERHELLODONE_H

#include "HsMessage.h"

class ServerHelloDone : public HSMessage {
public:
    ServerHelloDone() {}

    bool Encode(Packer *p) override { return true; }
    bool Decode(Packer *p, int len) override { return true; }
};

#endif //VTLS_SERVERHELLODONE_H
