
#ifndef VTLS_HANDSHAKE_H
#define VTLS_HANDSHAKE_H

#include "HsMessage.h"

class Handshake {
public:
    Handshake();
    virtual ~Handshake();

    static bool Parse(Packer* p, Handshake* handshake, bool* noEnoughData);
    static HSMessage* Build(Handshake* handshake, int hsType);

    inline int HandshakeType() const { return m_HSType; }
    HSMessage* Message() { return m_Message; }
    ByteBuffer* RawData() { return &m_RawData; }

    bool Encode(Packer* p);
    bool Decode(Packer* p);

private:
    HSMessage* NewMessage();

private:
    int        m_HSType;
    HSMessage* m_Message;

    ByteBuffer m_RawData;
};


#endif //VTLS_HANDSHAKE_H
