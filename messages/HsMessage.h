
#ifndef VTLS_HSMESSAGE_H
#define VTLS_HSMESSAGE_H

#include "../crypto/Crypto.h"
#include "../utils/TypeDefs.h"
#include "../utils/Packer.h"
#include "../Constants.h"

class HSMessage {
public:
    virtual ~HSMessage() {}

    virtual bool Encode(Packer* p) = 0;
    virtual bool Decode(Packer* p, int len) = 0;
};


#endif //VTLS_HSMESSAGE_H
