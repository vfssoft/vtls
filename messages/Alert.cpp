
#include "Alert.h"



Alert::Alert(): m_Level(0), m_Description(0) {}

const char* Alert::DescriptionStr() {
    return AlertDescriptionStr(m_Description);
}

const char* Alert::AlertDescriptionStr(int alertDesc) {
    switch (alertDesc) {
        case VTLS_ALERT_DESC_CLOSE_NOTIFY:            return "close notify";
        case VTLS_ALERT_DESC_UNEXPECTED_MESSAGE:      return "unexpected message";
        case VTLS_ALERT_DESC_BAD_RECORD_MAC:          return "bad record mac";
        case VTLS_ALERT_DESC_DECRYPTION_FAILED:       return "decryption failed";
        case VTLS_ALERT_DESC_RECORD_OVERFLOW:         return "record overflow";
        case VTLS_ALERT_DESC_DECOMPRESSION_FAILURE:   return "decompression failure";
        case VTLS_ALERT_DESC_HANDSHAKE_FAILURE:       return "handshake failure";
        case VTLS_ALERT_DESC_NO_CERTIFICATE:          return "no certificate";
        case VTLS_ALERT_DESC_BAD_CERTIFICATE:         return "bad certificate";
        case VTLS_ALERT_DESC_UNSUPPORTED_CERTIFICATE: return "unsupported certificate";
        case VTLS_ALERT_DESC_CERTIFICATE_REVOKED:     return "certificate revoked";
        case VTLS_ALERT_DESC_CERTIFICATE_EXPIRED:     return "certificate expired";
        case VTLS_ALERT_DESC_CERTIFICATE_UNKNOWN:     return "certificate unknown";
        case VTLS_ALERT_DESC_ILLEGAL_PARAMETER:       return "illegal parameter";
        case VTLS_ALERT_DESC_UNKNOWN_CA:              return "unknown ca";
        case VTLS_ALERT_DESC_ACCESS_DENIED:           return "access denied";
        case VTLS_ALERT_DESC_DECODE_ERROR:            return "decode error";
        case VTLS_ALERT_DESC_DECRYPT_ERROR:           return "decrypt error";
        case VTLS_ALERT_DESC_EXPORT_RESTRICTION:      return "export restriction";
        case VTLS_ALERT_DESC_PROTOCOL_VERSION:        return "protocol version";
        case VTLS_ALERT_DESC_INSUFFICIENT_SECURITY:   return "insufficient security";
        case VTLS_ALERT_DESC_INTERNAL_ERROR:          return "internal error";
        case VTLS_ALERT_DESC_USER_CANCELED:           return "user canceled";
        case VTLS_ALERT_DESC_NO_RENEGOTIATION:        return "no renegotiation";
        case VTLS_ALERT_DESC_UNSUPPORTED_EXTENSION:   return "unsupported extension";
    }
    return "unknown description value";
}