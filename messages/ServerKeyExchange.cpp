
#include "ServerKeyExchange.h"

ServerKeyExchange::ServerKeyExchange() {
}

bool ServerKeyExchange::Encode(Packer *p) {
    p->WriteUInt8(0x03); // named curve
    p->WriteUInt16(NamedCurve);
    p->WriteUInt8(PubKey.Length());
    p->WriteBytes(PubKey.Ptr(), PubKey.Length());

    p->WriteUInt16(SignatureAlgorithm);
    p->WriteUInt16(Signature.Length());
    p->WriteBytes(Signature.Ptr(), Signature.Length());
    return true;
}
bool ServerKeyExchange::Decode(Packer *p, int len) {
    int startPos = p->ReadPosition();
    int curveType = p->ReadUInt8();
    if (curveType != 0x03) return false; // We only support named curve.
    NamedCurve = p->ReadUInt16();

    PubKey.SetLength(p->ReadUInt8());
    p->ReadBytes(PubKey.Ptr(), PubKey.Length());

    ToSign.Set(p->Ptr() + startPos, 4 + PubKey.Length());

    SignatureAlgorithm = p->ReadUInt16();
    Signature.SetLength(p->ReadUInt16());
    p->ReadBytes(Signature.Ptr(), Signature.Length());

    return true;
}