
#ifndef VTLS_CERTIFICATE_H
#define VTLS_CERTIFICATE_H

#include "HsMessage.h"

#include "../utils/Array.h"
#include "../utils/ByteBuffer.h"


class Certificate : public HSMessage {
public:
    Certificate();

    bool Encode(Packer *p) override;
    bool Decode(Packer *p, int len) override;

    Array<ByteBuffer>* Certificates() { return &m_Certificates; }

private:
    Array<ByteBuffer> m_Certificates;
};


#endif //VTLS_CERTIFICATE_H
