

#ifndef VTLS_SERVERHELLO_H
#define VTLS_SERVERHELLO_H

#include "HsMessage.h"
#include "../utils/ValArr.h"

class ServerHello : public HSMessage {
public:
    int             Version;
    char            Random[32];
    ByteBuffer      SessionID; // 0 -> 32
    UINT16          CipherSuite;
    UINT8           CompressionMethod;

    ServerHello();

    bool Encode(Packer* p);
    bool Decode(Packer* p, int len);
};


#endif //VTLS_SERVERHELLO_H
