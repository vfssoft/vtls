

#include "ClientKeyExchange.h"


ClientKeyExchange::ClientKeyExchange() {

}

bool ClientKeyExchange::Encode(Packer *p) {
    p->WriteUInt8(PubKey.Length());
    p->WriteBytes(PubKey.Ptr(), PubKey.Length());
    return true;
}
bool ClientKeyExchange::Decode(Packer *p, int len) {
    PubKey.SetLength(p->ReadUInt8());
    p->ReadBytes(PubKey.Ptr(), PubKey.Length());
    return true;
}
