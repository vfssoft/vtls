
#include "Record.h"


bool Record::Encode(Packer* p) {
    int startPos = p->WritePosition();
    p->WriteUInt8(ContentType);
    p->WriteUInt16(ProtocolVersion);
    p->WriteUInt16(Fragment.Length());
    p->WriteBytes(Fragment.Ptr(), Fragment.Length());

    m_RawData.Set(p->Ptr() + startPos, p->WritePosition() - startPos);
    return true;
}
bool Record::Decode(Packer* p) {
    int startPos = p->ReadPosition();

    ContentType = p->ReadUInt8();
    ProtocolVersion = p->ReadUInt16();

    Fragment.SetLength(p->ReadUInt16());
    p->ReadBytes(Fragment.Ptr(), Fragment.Length());

    m_RawData.Set(p->Ptr() + startPos, p->ReadPosition() - startPos);
    return true;
}