
#ifndef VTLS_FINISHED_H
#define VTLS_FINISHED_H

#include "HsMessage.h"

class Finished : public HSMessage {
public:
    Finished();

    bool Encode(Packer *p) override;
    bool Decode(Packer *p, int len) override;

    ByteBuffer VerifyData;
};


#endif //VTLS_FINISHED_H
