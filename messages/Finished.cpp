
#include "Finished.h"

#define VTLS_VERIFY_DATA_LENGTH 12

Finished::Finished() {

}

bool Finished::Encode(Packer *p) {
    p->WriteBytes(VerifyData.Ptr(), VerifyData.Length());
    return true;
}
bool Finished::Decode(Packer *p, int len) {
    VerifyData.SetLength(VTLS_VERIFY_DATA_LENGTH);
    p->ReadBytes(VerifyData.Ptr(), VerifyData.Length());
    return true;
}
