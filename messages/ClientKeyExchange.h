

#ifndef VTLS_CLIENTKEYEXCHANGE_H
#define VTLS_CLIENTKEYEXCHANGE_H

#include "HsMessage.h"

class ClientKeyExchange : public HSMessage {
public:
    ClientKeyExchange();

    bool Encode(Packer *p) override;
    bool Decode(Packer *p, int len) override;

    ByteBuffer PubKey;
};


#endif //VTLS_CLIENTKEYEXCHANGE_H
