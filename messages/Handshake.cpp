#include "Handshake.h"

#include <cassert>

#include "ClientHello.h"
#include "ServerHello.h"
#include "Certificate.h"
#include "ServerKeyExchange.h"
#include "ServerHelloDone.h"
#include "ClientKeyExchange.h"
#include "Finished.h"


bool Handshake::Parse(Packer* p, Handshake* handshake, bool* noEnoughData) {
    // Check if there is enough bytes to parse a whole handshake message
    int rPos = p->ReadPosition();
    p->ReadUInt8(); // type, ignore
    int len = p->ReadUInt24();
    *noEnoughData = (p->Available() < len - 4);
    p->SetReadPosition(rPos); // reset the read position
    if (*noEnoughData) {
        return false;
    }

    return handshake->Decode(p);
}

HSMessage* Handshake::Build(Handshake* handshake, int hsType) {
    handshake->m_HSType = hsType;
    handshake->m_Message = handshake->NewMessage();
    return handshake->m_Message;
}

Handshake::Handshake() {
    m_HSType = 0;
    m_Message = NULL;
}
Handshake::~Handshake() {
    delete m_Message;
}

bool Handshake::Encode(Packer* p) {
    int startPos = p->WritePosition();
    p->WriteUInt8(HandshakeType());
    p->WriteUInt24(0); // Placeholder for Length

    if (!m_Message->Encode(p)) return false;

    p->SetUInt(startPos + 1, p->Available() - startPos - 4, 24);

    m_RawData.Set(p->Ptr() + startPos, p->WritePosition() - startPos);
    return true;
}
bool Handshake::Decode(Packer* p) {
    int startPos = p->ReadPosition();
    m_HSType = p->ReadUInt8();
    int len = p->ReadUInt24();

    m_Message = NewMessage();
    if (m_Message == NULL) return false;

    if (!m_Message->Decode(p, len)) return false;

    m_RawData.Set(p->Ptr() + startPos, p->ReadPosition() - startPos);
    return true;
}


HSMessage* Handshake::NewMessage() {
    switch (m_HSType) {
        case HSMT_HELLO_REUQEST:
            assert(false);
            return NULL;

        case HSMT_CLIENT_HELLO:
            return new ClientHello();

        case HSMT_SERVER_HELLO:
            return new ServerHello();

        case HSMT_NEW_SESSION_TICKET:
            assert(false);
            return NULL;

        case HSMT_END_OF_EARLY_DATA:
            assert(false);
            return NULL;

        case HSMT_ENCRYPTED_EXTENSIONS:
            assert(false);
            return NULL;

        case HSMT_CERTIFICATE:
            return new Certificate();

        case HSMT_SERVER_KEY_EXCHANGE:
            return new ServerKeyExchange();

        case HSMT_CERTIFICATE_REQUEST:
            assert(false);
            return NULL;

        case HSMT_SERVER_HELLO_DONE:
            return new ServerHelloDone();

        case HSMT_CERTIFICATE_VERIFY:
            assert(false);
            return NULL;

        case HSMT_CLIENT_KEY_EXCHANGE:
            return new ClientKeyExchange();

        case HSMT_FINISHED:
            return new Finished();

        case HSMT_CERTIFICATE_STATUS:
            assert(false);
            return NULL;

        case HSMT_KEY_UPDATE:
            assert(false);
            return NULL;

        default:
            return NULL; // unknown handshake message
    }
}

