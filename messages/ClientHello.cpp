

#include "ClientHello.h"

ClientHello::ClientHello(): Version(TLS_VERSION_1_2) {
    CompressionMethods.Add(COMPRESSION_NULL);
}

bool ClientHello::Encode(Packer *p) {
    p->WriteUInt16(Version);
    p->WriteBytes(Random, 32);

    p->WriteUInt8(SessionID.Length());
    p->WriteBytes(SessionID.Ptr(), SessionID.Length());

    p->WriteUInt16(2 * CipherSuites.Size());
    for (int i = 0; i < CipherSuites.Size(); i++) {
        p->WriteUInt16(CipherSuites.Get(i));
    }

    p->WriteUInt8(CompressionMethods.Size());
    for (int i = 0; i < CompressionMethods.Size(); i++) {
        p->WriteUInt8(CompressionMethods.Get(i));
    }

    Packer extPacker;
    if (ExtSupportedGroups.Size()) {
        extPacker.WriteUInt16(EXT_TYPE_SUPPORTED_GROUPS); // Type
        extPacker.WriteUInt16(2 + ExtSupportedGroups.Size() * 2); // Length
        extPacker.WriteUInt16(ExtSupportedGroups.Size() * 2); // Supported Groups List Length
        for (int i = 0; i < ExtSupportedGroups.Size(); i++) {
            extPacker.WriteUInt16(ExtSupportedGroups.Get(i));
        }

        extPacker.WriteUInt16(EXT_TYPE_EC_POINT_FORMATS); // Type
        extPacker.WriteUInt16(2); // Length
        extPacker.WriteUInt8(1); // EC point formats Length
        extPacker.WriteUInt8(0); // uncompressed (0)
    }
    if (ExtSignatureAlgorithms.Size() > 0) {
        extPacker.WriteUInt16(EXT_TYPE_SIGNATURE_ALGORITHM); // Type
        extPacker.WriteUInt16(2 + ExtSignatureAlgorithms.Size() * 2); // Length
        extPacker.WriteUInt16(ExtSignatureAlgorithms.Size() * 2); // Signature Hash Algorithm Length
        for (int i = 0; i < ExtSignatureAlgorithms.Size(); i++) {
            extPacker.WriteUInt16(ExtSignatureAlgorithms.Get(i));
        }
    }

    if (extPacker.Available()) {
        p->WriteUInt16(extPacker.Available());
        p->WriteBytes(extPacker.Ptr(), extPacker.Available());
    }

    return true;
}

bool ClientHello::Decode(Packer *p, int len) {
    int pktLen = len;
    Version = p->ReadUInt16();
    p->ReadBytes(Random, 32);

    len = p->ReadUInt8();
    SessionID.SetLength(len);
    if (len > 0) {
        p->ReadBytes(SessionID.Ptr(), len);
    }

    len = p->ReadUInt16();
    CipherSuites.SetSize(len / 2);
    for (int i = 0; i < CipherSuites.Size(); i++) {
        CipherSuites.Set(i, p->ReadUInt16());
    }

    len = p->ReadUInt8();
    CompressionMethods.SetSize(len);
    for (int i = 0; i < CompressionMethods.Size(); i++) {
        CompressionMethods.Set(i, p->ReadInt8());
    }
    return true;
}
