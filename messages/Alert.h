
#ifndef VTLS_ALERT_H
#define VTLS_ALERT_H

#define VTLS_ALERT_LEVEL_WARNING 1
#define VTLS_ALERT_LEVEL_FATAL   2

#define VTLS_ALERT_DESC_CLOSE_NOTIFY            0
#define VTLS_ALERT_DESC_UNEXPECTED_MESSAGE      10
#define VTLS_ALERT_DESC_BAD_RECORD_MAC          20
#define VTLS_ALERT_DESC_DECRYPTION_FAILED       21
#define VTLS_ALERT_DESC_RECORD_OVERFLOW         22
#define VTLS_ALERT_DESC_DECOMPRESSION_FAILURE   30
#define VTLS_ALERT_DESC_HANDSHAKE_FAILURE       40
#define VTLS_ALERT_DESC_NO_CERTIFICATE          41
#define VTLS_ALERT_DESC_BAD_CERTIFICATE         42
#define VTLS_ALERT_DESC_UNSUPPORTED_CERTIFICATE 43
#define VTLS_ALERT_DESC_CERTIFICATE_REVOKED     44
#define VTLS_ALERT_DESC_CERTIFICATE_EXPIRED     45
#define VTLS_ALERT_DESC_CERTIFICATE_UNKNOWN     46
#define VTLS_ALERT_DESC_ILLEGAL_PARAMETER       47
#define VTLS_ALERT_DESC_UNKNOWN_CA              48
#define VTLS_ALERT_DESC_ACCESS_DENIED           49
#define VTLS_ALERT_DESC_DECODE_ERROR            50
#define VTLS_ALERT_DESC_DECRYPT_ERROR           51
#define VTLS_ALERT_DESC_EXPORT_RESTRICTION      60
#define VTLS_ALERT_DESC_PROTOCOL_VERSION        70
#define VTLS_ALERT_DESC_INSUFFICIENT_SECURITY   71
#define VTLS_ALERT_DESC_INTERNAL_ERROR          80
#define VTLS_ALERT_DESC_USER_CANCELED           90
#define VTLS_ALERT_DESC_NO_RENEGOTIATION        100
#define VTLS_ALERT_DESC_UNSUPPORTED_EXTENSION   110

class Alert {
public:
    Alert();

    void Set(int level, int desc) {
        m_Level = level;
        m_Description = desc;
    }

    int Level() const { return m_Level; }
    int Description() const { return m_Description; }
    const char* DescriptionStr();

    static const char* AlertDescriptionStr(int alertDesc);

private:
    int m_Level;
    int m_Description;
};


#endif //VTLS_ALERT_H
