
#include "Certificate.h"

Certificate::Certificate() {

}

bool Certificate::Encode(Packer *p) {
    int lenPos = p->WritePosition();
    p->WriteUInt24(0); // length placeholder

    int certsLen = 0;
    for (int i = 0; i < m_Certificates.Size(); i++) {
        ByteBuffer* c = m_Certificates.Get(i);
        certsLen += c->Length();
        p->WriteUInt24(c->Length());
        p->WriteBytes(c->Ptr(), c->Length());
    }

    p->SetUInt(lenPos, certsLen, 24);
    return true;
}
bool Certificate::Decode(Packer *p, int len) {
    int startPos = p->ReadPosition();
    int certsLen = p->ReadUInt24();

    while (p->ReadPosition() - startPos < certsLen) {
        int certLen = p->ReadInt24();
        ByteBuffer* c = m_Certificates.Add();
        c->SetLength(certLen);
        p->ReadBytes(c->Ptr(), c->Length());
    }
    return true;
}