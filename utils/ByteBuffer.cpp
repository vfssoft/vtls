//
// Created by Martin on 12/25/2023.
//

#include "ByteBuffer.h"

#include <cassert>
#include <cstdlib>
#include <cstring>


ByteBuffer::ByteBuffer() {
    m_Buffer      = NULL;
    m_Cap         = 0;
    m_Length      = 0;
}

ByteBuffer::~ByteBuffer() {
    this->Reset();
}

void ByteBuffer::SetStr(const char* str) {
    Set(str, strlen(str));
}

void ByteBuffer::Set(const char* data, int len) {
    SetLength(len);
    memcpy(m_Buffer, data, len);
}
void ByteBuffer::Set(int index, const char* data, int len) {
    assert(index >= 0 && index + len <= Length());
    memcpy(m_Buffer + index, data, len);
}
void ByteBuffer::Set(int index, char val) {
    assert(index >= 0 && index < Length());
    m_Buffer[index] = val;
}
void ByteBuffer::Set(ByteBuffer* buf) {
    Set(buf->Ptr(), buf->Length());
}
void ByteBuffer::AppendStr(const char* str) {
    Append(str, strlen(str));
}
void ByteBuffer::Append(const char* data, int len) {
    SetLength(m_Length + len);
    Set(m_Length - len, data, len);
}
void ByteBuffer::Append(ByteBuffer* buf) {
    Append(buf->Ptr(), buf->Length());
}
void ByteBuffer::AppendChar(char c) {
    char oneChar[1] = { c };
    return Append(oneChar, 1);
}

void ByteBuffer::SetLength(int len) {
    EnsureCap(len);
    m_Length = len;
}

void ByteBuffer::SetHex(const char* data, int len) {
    Reset();
    AppendHex(data, len);
}
void ByteBuffer::AppendHex(const char* data, int len) {
    ByteBuffer buf;
    buf.SetLength(len * 2);
    ToHex(data, len, buf.Ptr());
    AppendStr(buf.Ptr());
}
void ByteBuffer::ToHex() {
    ByteBuffer buf;
    buf.SetLength(m_Length * 2);
    ToHex(Ptr(), Length(), buf.Ptr());
    Set(buf.Ptr(), buf.Length());
}

void ByteBuffer::SetHexDecoded(const char* hexStr) {
    Reset();
    AppendHexDecoded(hexStr);
}
void ByteBuffer::AppendHexDecoded(const char* hexStr) {
    ByteBuffer buf;
    buf.SetLength(strlen(hexStr) / 2);
    FromHex(hexStr, buf.Ptr());
    Append(buf.Ptr(), buf.Length());
}
void ByteBuffer::HexDecoded() {
    ByteBuffer buf;
    buf.SetLength(m_Length / 2);
    FromHex(Ptr(), buf.Ptr());
    Set(buf.Ptr(), buf.Length());
}

void ByteBuffer::EnsureCap(int cap) {
    if (cap <= m_Cap) return;

    int newCap = 0;
    if (cap < 1024) {
        newCap = ((cap + 63) / 64 * 64) * 4;
    } else if (cap < 1024 * 1024) {
        newCap = ((cap + 1023) / 1024) * 1024 * 4;
    } else {
        while (newCap <= cap) newCap += 1024 * 1024 * 64;
    }

    char* newBuf = new char[newCap];
    assert(newBuf != NULL);

    if (m_Cap > 0) {
        memcpy(newBuf, m_Buffer, m_Cap);
        delete m_Buffer;
    }
    m_Buffer = newBuf;

    memset(m_Buffer + m_Length, 0, newCap - m_Length);
    m_Cap = newCap;
}

void ByteBuffer::Reset() {
    if (m_Cap) {
        delete [] m_Buffer;
    }

    m_Buffer = NULL;
    m_Cap    = 0;
    m_Length = 0;
}

void ByteBuffer::Delete(int start, int len) {
    assert(start >= 0);
    assert(len >= 0);
    assert(start + len <= m_Length);

    memmove(m_Buffer + start, m_Buffer + start + len, m_Length - start - len);
    SetLength(m_Length - len);
}

void ByteBuffer::Sub(int start, int len) {
    assert(start >= 0);
    assert(len >= 0);
    assert(start + len <= m_Length);

    memmove(m_Buffer, m_Buffer + start, len);
    SetLength(len);
}

int ByteBuffer::IndexOf(const char* ptr, int len, int start) {
    assert(start >= 0);
    assert(len >= 0);

    for (int i = start; i < m_Length - len; i++) {
        if (Equals(ptr, m_Buffer + i, len)) {
            return i;
        }
    }
    return -1;
}
int ByteBuffer::LastIndexOf(const char* ptr, int len) {
    assert(len >= 0);
    int endIndex = Length() - len;
    for (int i = endIndex; i >= 0; i--) {
        if (Equals(ptr, m_Buffer + endIndex, len)) {
            return endIndex;
        }
    }
    return -1;
}
int ByteBuffer::IndexOf(const char c, int start) {
    assert(start >= 0);
    assert(start < Length());

    for (int i = start; i < Length(); i++) {
        if (m_Buffer[i] == c)
            return i;
    }
    return -1;

}
int ByteBuffer::LastIndexOf(const char c) {
    return LastIndexOf(c, 0, Length());
}
int ByteBuffer::LastIndexOf(const char c, int start, int len) {
    assert(start >= 0);
    assert(len >= 0);
    assert(start + len <= Length());
    for (int i = start + len - 1; i >= start; i--) {
        if (m_Buffer[i] == c)
            return i;
    }
    return -1;
}

bool ByteBuffer::StartWith(const char* ptr, int len) {
    assert(len >= 0);

    if (len > Length())
        return false;

    return Equals(ptr, m_Buffer, len);
}
bool ByteBuffer::EndWith(const char* ptr, int len) {
    assert(len >= 0);

    if (len > Length())
        return false;

    return Equals(ptr, m_Buffer + m_Length - len, len);
}
bool ByteBuffer::StartWith(const char c) {
    return Length() == 0 ? false : m_Buffer[0] == c;
}
bool ByteBuffer::EndWith(const char c) {
    return Length() == 0 ? false : m_Buffer[Length()-1] == c;
}
bool ByteBuffer::Contains(const char* ptr, int len) {
    if (len == 0) return false;

    for (int i = 0; i < m_Length - len; i++) {
        if (m_Buffer[i] == ptr[0]) {
            if (Equals(m_Buffer + i, ptr, len)) {
                return true;
            }
        }
    }
    return false;
}
bool ByteBuffer::Equals(const char* data, int len) {
    if (len != Length()) return false;
    return Equals(Ptr(), data, len);
}
bool ByteBuffer::Equals(ByteBuffer* other) {
    return Equals(other->Ptr(), other->Length());
}

bool ByteBuffer::Equals(const char* p1, const char* p2, int len) {
    for (int i = 0; i < len; i++) {
        if (p1[i] != p2[i]) return false;
    }
    return true;
}

void ByteBuffer::ToHex(const char* data, int len, char* result) {
    const char *hex = "0123456789abcdef";

    for (int i = 0; i < len; i++) {
        unsigned int c    = data[i] & 0xFF;
        result[i * 2]     = hex[(c & 0xF0) >> 4];
        result[i * 2 + 1] = hex[c & 0x0F];
    }
}
void ByteBuffer::FromHex(const char* hexStr, char* result) {
    int len = strlen(hexStr);
    for (int i = 0; i < len / 2; i++) {
        int h = HexDigitToInt(hexStr[2 * i]) & 0x0F;
        int l = HexDigitToInt(hexStr[2 * i + 1]) & 0x0F;
        result[i] = (h << 4) | l;
    }
}
int ByteBuffer::HexDigitToInt(char digit) {
    if (digit >= '0' && digit <= '9') {
        return digit - '0';
    } else if (digit >= 'a' && digit <= 'f') {
        return digit - 'a' + 10;
    } else if (digit >= 'A' && digit <= 'F') {
        return digit - 'A' + 10;
    } else {
        return -1;
    }
}
