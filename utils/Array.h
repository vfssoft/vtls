
#ifndef VTLS_ARRAY_H
#define VTLS_ARRAY_H

#include <cassert>

template<typename T>
class Array {

public:
    Array(int size=0);
    virtual ~Array();

    T*   Add();
    T*   Get(int idx);
    bool Delete(int idx);
    void Reset();

    inline bool Empty() { return m_Size == 0; }
    inline int Size() { return m_Size; }
    void SetSize(int size);

private:
    void EnsureSize(int size);

private:
    T** m_Array;
    int m_Size;
    int m_Cap;
};

template <typename T>
Array<T>::Array(int size) {
    m_Array = 0;
    m_Size = 0;
    m_Cap = 0;
    this->EnsureSize(size);
}

template <typename T>
Array<T>::~Array() {
    this->Reset();
}

template <typename T>
T* Array<T>::Add() {
    this->EnsureSize(m_Size + 1);
    m_Size++;
    m_Array[m_Size-1] = new T();
    assert(m_Array[m_Size-1] != NULL);
    return m_Array[m_Size-1];
}

template <typename T>
T* Array<T>::Get(int idx) {
    if (idx < 0 || idx >= m_Size) {
        return NULL;
    }

    return m_Array[idx];
}

template <typename T>
bool Array<T>::Delete(int idx) {
    if (idx < 0 || idx >= m_Size) {
        return false;
    }

    if (m_Array[idx] != NULL)
        delete m_Array[idx];

    for (int i = idx; i < m_Size - 1; i++) {
        m_Array[i] = m_Array[i+1];
    }
    m_Size--;
    m_Array[m_Size] = NULL;

    return true;
}

template <typename T>
void Array<T>::SetSize(int size) {
    this->EnsureSize(size);

    for (int i = m_Size; i < size; i++) {
        m_Array[i] = new T();
        assert(m_Array[i] != NULL);
    }

    m_Size = size;
}

template <typename T>
void Array<T>::Reset() {
    if (m_Array != NULL) {
        for (int i = 0; i < m_Size; i++) {
            if (m_Array[i]) delete m_Array[i];
        }
        delete[] m_Array;
    }
    m_Array = NULL;
    m_Size = 0;
    m_Cap = 0;
}

template <typename T>
void Array<T>::EnsureSize(int size) {
    if (size <= m_Cap) return;

    int newCap = m_Cap == 0 ? 32 : m_Cap;
    while (newCap < size) { newCap = newCap << 1; }

    T** newArray = new T*[newCap];
    assert(newArray != NULL);

    for (int i = 0; i < newCap; i++) {
        if (i < m_Size) {
            newArray[i] = m_Array[i];
        } else {
            newArray[i] = NULL;
        }
    }

    if (m_Array != NULL) {
        // Don't free the old element's memories, they are using by the newArray
        delete[] m_Array;
        m_Array = NULL;
    }

    m_Array = newArray;
    m_Cap = newCap;
    // This method doesn't change the m_Size
}


#endif //VTLS_ARRAY_H
