

#ifndef VTLS_BYTEBUFFER_H
#define VTLS_BYTEBUFFER_H


class ByteBuffer {
public:
    ByteBuffer();
    ~ByteBuffer();

    inline bool  Empty() { return m_Length == 0; }
    inline char* Ptr(int off=0) { return m_Length == 0 ? (char*)"" : m_Buffer + off; }
    inline int   Length() { return m_Length; }

    void SetStr(const char* str);
    void Set(const char* data, int len);
    void Set(int index, const char* data, int len);
    void Set(int index, char val);
    void Set(ByteBuffer* buf);
    void AppendStr(const char* str);
    void Append(const char* data, int len);
    void Append(ByteBuffer* buf);
    void AppendChar(char c);
    void SetLength(int len);

    void SetHex(const char* data, int len);
    void AppendHex(const char* data, int len);
    void ToHex();
    void SetHexDecoded(const char* hexStr);
    void AppendHexDecoded(const char* hexStr);
    void HexDecoded();

    void Delete(int start, int len);
    void Sub(int start, int len);

    int IndexOf(const char* ptr, int len, int start=0);
    int LastIndexOf(const char* ptr, int len);
    int IndexOf(const char c, int start=0);
    int LastIndexOf(const char c);
    int LastIndexOf(const char c, int start, int len);

    bool StartWith(const char* ptr, int len);
    bool EndWith(const char* ptr, int len);
    bool StartWith(const char c);
    bool EndWith(const char c);
    bool Contains(const char* ptr, int len);
    bool Equals(const char* data, int len);
    bool Equals(ByteBuffer* other);

    void Reset();

    static void ToHex(const char* data, int len, char* result);
    static void FromHex(const char* hexStr, char* result);
    static int  HexDigitToInt(char digit);

private:
    void EnsureCap(int cap);
    bool Equals(const char* p1, const char* p2, int len);

private:
    char* m_Buffer;
    int   m_Cap;
    int   m_Length;
};



#endif //VTLS_BYTEBUFFER_H
