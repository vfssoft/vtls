
#ifndef VTLS_VALARR_H
#define VTLS_VALARR_H

#include <cassert>

template<typename T>
class ValArr {

public:
    ValArr(int size=0);
    virtual ~ValArr();

    void Add(T val);
    void Set(int idx, T val);
    T    Get(int idx);
    bool Delete(int idx);
    void Reset();

    inline bool Empty() { return m_Size == 0; }
    inline int Size() { return m_Size; }
    void SetSize(int size);

private:
    void EnsureSize(int size);

private:
    T*  m_Array;
    int m_Size;
    int m_Cap;
};

template <typename T>
ValArr<T>::ValArr(int size) {
    m_Array = NULL;
    m_Size = 0;
    m_Cap = 0;
    this->EnsureSize(size);
}

template <typename T>
ValArr<T>::~ValArr() {
    this->Reset();
}

template <typename T>
void ValArr<T>::Add(T val) {
    this->EnsureSize(m_Size + 1);
    m_Size++;
    m_Array[m_Size-1] = val;
}

template <typename T>
void ValArr<T>::Set(int idx, T val) {
    assert(idx >= 0 && idx < m_Size);
    m_Array[idx] = val;
}

template <typename T>
T ValArr<T>::Get(int idx) {
    if (idx < 0 || idx >= m_Size) {
        return 0;
    }

    return m_Array[idx];
}

template <typename T>
bool ValArr<T>::Delete(int idx) {
    if (idx < 0 || idx >= m_Size) {
        return false;
    }

    for (int i = idx; i < m_Size - 1; i++) {
        m_Array[i] = m_Array[i+1];
    }
    m_Size--;
    m_Array[m_Size] = 0;

    return true;
}

template <typename T>
void ValArr<T>::SetSize(int size) {
    this->EnsureSize(size);

    for (int i = m_Size; i < size; i++) {
        m_Array[i] = 0;
    }

    m_Size = size;
}

template <typename T>
void ValArr<T>::Reset() {
    if (m_Array != NULL) {
        delete[] m_Array;
    }
    m_Array = NULL;
    m_Size = 0;
    m_Cap = 0;
}

template <typename T>
void ValArr<T>::EnsureSize(int size) {
    if (size <= m_Cap) return;

    int newCap = m_Cap == 0 ? 32 : m_Cap;
    while (newCap < size) { newCap = newCap << 1; }

    T* newArray = new T[newCap];
    assert(newArray != NULL);

    for (int i = 0; i < newCap; i++) {
        if (i < m_Size) {
            newArray[i] = m_Array[i];
        } else {
            newArray[i] = 0;
        }
    }

    if (m_Array != NULL) {
        delete[] m_Array;
        m_Array = NULL;
    }

    m_Array = newArray;
    m_Cap = newCap;
    // This method doesn't change the m_Size
}


#endif //VTLS_VALARR_H
