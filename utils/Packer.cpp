#include "Packer.h"

#include <cstdlib>
#include <cstring>

Packer::Packer(): m_ROffset(0), m_BigEndian(true) {

}
Packer::Packer(ByteBuffer* buf): m_ROffset(0), m_BigEndian(true) {
    WriteBytes(buf->Ptr(), buf->Length());
}

void Packer::WriteInt8(INT8 val) {
    WriteInt(val, 8);
}
void Packer::WriteInt16(INT16 val) {
    WriteInt(val, 16);
}
void Packer::WriteInt24(INT32 val) {
    WriteInt(val, 24);
}
void Packer::WriteInt32(INT32 val) {
    WriteInt(val, 32);
}
void Packer::WriteInt48(INT64 val) {
    WriteInt(val, 48);
}
void Packer::WriteInt64(INT64 val) {
    WriteInt(val, 64);
}

void Packer::WriteUInt8(UINT8 val) {
    WriteUInt(val, 8);
}
void Packer::WriteUInt16(UINT16 val) {
    WriteUInt(val, 16);
}
void Packer::WriteUInt24(UINT32 val) {
    WriteUInt(val, 24);
}
void Packer::WriteUInt32(UINT32 val) {
    WriteUInt(val, 32);
}
void Packer::WriteUInt48(UINT64 val) {
    WriteUInt(val, 48);
}
void Packer::WriteUInt64(UINT64 val) {
    WriteUInt(val, 64);
}
void Packer::WriteInt(INT64 val, int bits) {
    SignedNumToBytes(val, m_NumBuf, bits/8);
    WriteBytes(m_NumBuf, bits/8);
}
void Packer::WriteUInt(UINT64 val, int bits) {
    UnsignedNumToBytes(val, m_NumBuf, bits/8);
    WriteBytes(m_NumBuf, bits/8);
}
void Packer::WriteBytes(const char* buf, int len) {
    m_Buffer.Append(buf, len);
}

void Packer::SetUInt(int index, UINT64 val, int bits) {
    UnsignedNumToBytes(val, m_NumBuf, bits / 8);
    m_Buffer.Set(index, m_NumBuf, bits / 8);
}


INT8 Packer::ReadInt8() {
    return ReadInt(8);
}
INT16 Packer::ReadInt16() {
    return ReadInt(16);
}
INT32 Packer::ReadInt24() {
    return ReadInt(24);
}
INT32 Packer::ReadInt32() {
    return ReadInt(32);
}
INT64 Packer::ReadInt48() {
    return ReadInt(48);
}
INT64 Packer::ReadInt64() {
    return ReadInt(64);
}
UINT8 Packer::ReadUInt8() {
    return ReadUInt(8);
}
UINT16 Packer::ReadUInt16() {
    return ReadUInt(16);
}
UINT32 Packer::ReadUInt24() {
    return ReadUInt(24);
}
UINT32 Packer::ReadUInt32() {
    return ReadUInt(32);
}
UINT64 Packer::ReadUInt48() {
    return ReadUInt(48);
}
UINT64 Packer::ReadUInt64() {
    return ReadUInt(64);
}
INT64 Packer::ReadInt(int bits) {
    int l = ReadBytes(m_NumBuf, bits/8);
    return l < bits/8 ? 0 : BytesToSignedNum(m_NumBuf, l);
}
UINT64 Packer::ReadUInt(int bits) {
    int l = ReadBytes(m_NumBuf, bits/8);
    return l < bits/8 ? 0 : BytesToUnsignedNum(m_NumBuf, l);
}

int Packer::ReadBytes(char* buf, int len) {
    int bytesRead = len > Available() ? Available() : len;
    memcpy(buf, m_Buffer.Ptr(m_ROffset), bytesRead);
    m_ROffset += bytesRead;
    return bytesRead;
}

void Packer::SignedNumToBytes(INT64 val, char* buf, int bytesCount) {
    if (m_BigEndian) {
        for (int i = bytesCount - 1; i >= 0; i--) {
            buf[i] = val & 0xFF;
            val = val >> 8;
        }
    }
}
void Packer::UnsignedNumToBytes(UINT64 val, char* buf, int bytesCount) {
    if (m_BigEndian) {
        for (int i = bytesCount - 1; i >= 0; i--) {
            buf[i] = val & 0xFF;
            val = val >> 8;
        }
    }
}
INT64 Packer::BytesToSignedNum(char* buf, int len) {
    INT64 val = 0;
    if (m_BigEndian) {
        for (int i = 0; i < len; i++) {
            val = (val << 8) | (buf[i] & 0xff);
        }
    }
    return val;
}
UINT64 Packer::BytesToUnsignedNum(char* buf, int len) {
    UINT64 val = 0;
    if (m_BigEndian) {
        for (int i = 0; i < len; i++) {
            val = (val << 8) | (buf[i] & 0xff);
        }
    }
    return val;
}


int Packer::Available() {
    return m_Buffer.Length() - m_ROffset;
}

void Packer::Reset() {
    m_Buffer.Reset();
    m_ROffset = 0;
}