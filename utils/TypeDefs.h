
#ifndef VTLS_TYPEDEFS_H
#define VTLS_TYPEDEFS_H

#ifdef WIN32
#include <Windows.h>
#undef X509_NAME
#endif

#ifndef INT8
#define INT8 char
#endif

#ifndef UINT8
#define UINT8 unsigned char
#endif

#ifndef INT16
#define INT16 short
#endif

#ifndef UINT16
#define UINT16 unsigned short
#endif

#ifndef INT32
#define INT32 int
#endif

#ifndef UINT32
#define UINT32 unsigned int
#endif

#ifndef INT64
#define INT64 long long
#endif

#ifndef UINT64
#define UINT64 unsigned long long
#endif

#endif //VTLS_TYPEDEFS_H
