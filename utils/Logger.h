
#ifndef VTLS_LOGGER_H
#define VTLS_LOGGER_H

#define VTLS_LOG_NONE  0
#define VTLS_LOG_ERROR 1
#define VTLS_LOG_INFO  2
#define VTLS_LOG_VERB  3

#define VTLS_LOG_FLAGS_LOG_LEVEL (1 << 1)
#define VTLS_LOG_FLAGS_THREAD_ID (1 << 2)
#define VTLS_LOG_FLAGS_TIMESTAMP (1 << 3)
#define VTLS_LOG_FLAGS_MESSAGES  (1 << 4)
#define VTLS_LOG_FLAGS_HEX_DUMP  (1 << 5)
#define VTLS_LOG_FLAGS_INSTANCE  (1 << 6)

// TODO: key log file

#include <cstdio>
#include <cstdarg>
#include <cstring>
#include <ctime>

#include "TypeDefs.h"
#include "ByteBuffer.h"

typedef void (*VtlsLogCallback)(const char* log, void* ctx);

class Logger {
public:
    Logger();

    inline int LogLevel() { return m_LogLevel; }
    inline void SetLogLevel(int val) { m_LogLevel = val; }

    inline int Flags() { return m_Flags; }
    inline void SetFlags(int val) { m_Flags = val; }

    inline void SetLogCallback(VtlsLogCallback callback, void* ctx) {
        m_Callback = callback;
        m_CallbackCtx = ctx;
    }

    inline void AttachInstance(void* instance) {
        m_Instance = (UINT64)instance;
        if (instance == NULL) {
            m_Flags |= VTLS_LOG_FLAGS_INSTANCE;
        } else {
            m_Flags &= ~VTLS_LOG_FLAGS_INSTANCE;
        }
    }

    void LogInfo(const char* fmt, ...);
    void LogError(const char* fmt, ...);
    void LogVerb(const char* fmt, ...);

    void LogHexDump(const char* data, int lenData, bool output);

private:
    void Log(int level, const char* fmt, va_list args);
    void OutLog(const char* msg);

    const char* LogPrefix(int level);
    static UINT32 ThreadId();
    static UINT32 Timestamp();

    static void ToHexDump(const char* data, int lenData, ByteBuffer* buf);
    const char* CurrentTime();

private:
    int             m_LogLevel;
    int             m_Flags;
    UINT64          m_Instance;
    void*           m_CallbackCtx;
    VtlsLogCallback m_Callback;

    char   m_Prefix[256];
    char   m_LogBuf[4096];
    char   m_TimeBuf[64];
};


#endif //VTLS_LOGGER_H
