

#include "Logger.h"

const char* LOG_LEVEL_STR[] = {
        "NONE",
        "ERRO",
        "INFO ",
        "VERB",
};

#define VTLS_LOG_TIME_FORMAT "%Y-%m-%d %H:%M:%S"

Logger::Logger() {
    m_LogLevel = VTLS_LOG_INFO;
    m_Flags = /*VTLS_LOG_FLAGS_INSTANCE |*/ VTLS_LOG_FLAGS_LOG_LEVEL | VTLS_LOG_FLAGS_THREAD_ID | VTLS_LOG_FLAGS_TIMESTAMP | VTLS_LOG_FLAGS_MESSAGES | VTLS_LOG_FLAGS_HEX_DUMP;
    m_Instance = NULL;

    m_Callback = NULL;
    m_CallbackCtx;
}

void Logger::LogInfo(const char* fmt, ...) {
    va_list st;
    va_start(st, fmt);
    Log(VTLS_LOG_INFO, fmt, st);
    va_end(st);
}

void Logger::LogError(const char* fmt, ...) {
    va_list st;
    va_start(st, fmt);
    Log(VTLS_LOG_ERROR, fmt, st);
    va_end(st);
}
void Logger::LogVerb(const char* fmt, ...) {
    va_list st;
    va_start(st, fmt);
    Log(VTLS_LOG_VERB, fmt, st);
    va_end(st);
}

void Logger::Log(int level, const char* fmt, va_list args) {
    if (level > m_LogLevel) return;

    int offset = 0;
    const char* prefix = LogPrefix(level);
    if (prefix[0] != 0) {
        offset = strlen(prefix);
        strcpy(m_LogBuf, prefix);
    }

    vsprintf(m_LogBuf + offset, fmt, args);

    OutLog(m_LogBuf);
}
void Logger::OutLog(const char* msg) {
    if (m_Callback) {
        m_Callback(msg, m_CallbackCtx);
    } else {
        printf("%s\n", msg);
    }
}

const char* Logger::LogPrefix(int level) {
    // Format: [LOG_LEVEL][INSTANCE][THREAD_ID][TIMESTAMP]
    bool logLevelEnabled  = m_Flags & VTLS_LOG_FLAGS_LOG_LEVEL;
    bool instanceEnabled  = m_Flags & VTLS_LOG_FLAGS_INSTANCE;
    bool threadIdEnabled  = m_Flags & VTLS_LOG_FLAGS_THREAD_ID;
    bool timestampEnabled = m_Flags & VTLS_LOG_FLAGS_TIMESTAMP;

    if (instanceEnabled) {
        if (logLevelEnabled && threadIdEnabled && timestampEnabled) {
            sprintf_s(m_Prefix, "[%s][%016I64x][%08x][%08x] ", LOG_LEVEL_STR[level], m_Instance, ThreadId(), Timestamp());
        } else if (logLevelEnabled && threadIdEnabled) {
            sprintf_s(m_Prefix, "[%s][%016I64x][%08x] ", LOG_LEVEL_STR[level], m_Instance, ThreadId());
        } else if (logLevelEnabled && timestampEnabled) {
            sprintf_s(m_Prefix, "[%s][%016I64x][%08x] ", LOG_LEVEL_STR[level], m_Instance, Timestamp());
        } else if (threadIdEnabled && timestampEnabled) {
            sprintf_s(m_Prefix, "[%016I64x][%08x][%08x] ", m_Instance, ThreadId(), Timestamp());
        } else if (threadIdEnabled) {
            sprintf_s(m_Prefix, "[%016I64x][%08x] ", m_Instance, ThreadId());
        } else if (timestampEnabled) {
            sprintf_s(m_Prefix, "[%016I64x][%08x] ", m_Instance, Timestamp());
        } else {
            sprintf_s(m_Prefix, "[%016I64x] ", m_Instance);
        }
    } else {
        if (logLevelEnabled && threadIdEnabled && timestampEnabled) {
            sprintf_s(m_Prefix, "[%s][%08x][%08x] ", LOG_LEVEL_STR[level], ThreadId(), Timestamp());
        } else if (logLevelEnabled && threadIdEnabled) {
            sprintf_s(m_Prefix, "[%s][%08x] ", LOG_LEVEL_STR[level], ThreadId());
        } else if (logLevelEnabled && timestampEnabled) {
            sprintf_s(m_Prefix, "[%s][%08x] ", LOG_LEVEL_STR[level], Timestamp());
        } if (threadIdEnabled && timestampEnabled) {
            sprintf_s(m_Prefix, "[%08x][%08x] ", ThreadId(), Timestamp());
        } else if (threadIdEnabled) {
            sprintf_s(m_Prefix, "[%08x] ", ThreadId());
        } else if (timestampEnabled) {
            sprintf_s(m_Prefix, "[%08x] ",Timestamp());
        } else {
            m_Prefix[0] = 0;
        }
    }
    return m_Prefix;
}

UINT32 Logger::ThreadId() {
#ifdef WIN32
    return GetCurrentThreadId();
#endif
}
UINT32 Logger::Timestamp() {
#ifdef WIN32
    return GetTickCount64();
#endif
}

void Logger::LogHexDump(const char* data, int lenData, bool output) {
    // ref: https://www.wireshark.org/docs/wsug_html_chunked/ChIOImportSection.html
    ByteBuffer dump;

    // I 2019-05-14T19:04:57Z
    dump.AppendStr(output ? "O " : "I ");
    dump.AppendStr(CurrentTime());
    dump.AppendStr("\r\n");

    // Hex dump
    ToHexDump(data, lenData, &dump);

    OutLog(dump.Ptr());
}
void Logger::ToHexDump(const char* data, int lenData, ByteBuffer* buf) {
    if (lenData == 0) return;

    const char* hexDigits = "0123456789abcdef";
    char curLine[68];
    char lineNoBuf[12];
    curLine[67] = 0;

    int i = 0;
    while (i < lenData) {
        memset(curLine, ' ', 67);
        for (int j = 0; j < 16 && i < lenData; j++) {
            char c = data[i++];

            curLine[j * 3]     = hexDigits[(c & 0xf0) >> 4];
            curLine[j * 3 + 1] = hexDigits[(c & 0x0f)];
            curLine[51 + j]    = (c < 0x20 || c > 0x7E) ? '.' : c;
        }

        sprintf(lineNoBuf, "%08x  ", (i - 1) / 16 * 16);
        buf->AppendStr(lineNoBuf);
        buf->AppendStr(curLine);
        if (i < lenData) buf->AppendStr("\r\n");
    }
}

const char* Logger::CurrentTime() {
    time_t rawTime;
    struct tm* timeInfo;

    time(&rawTime);
    timeInfo = localtime(&rawTime);
    strftime(m_TimeBuf, 64, VTLS_LOG_TIME_FORMAT, timeInfo);
    return m_TimeBuf;
}