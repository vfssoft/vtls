
#ifndef VTLS_PACKER_H
#define VTLS_PACKER_H

#include "TypeDefs.h"
#include "ByteBuffer.h"

class Packer {
public:
    Packer();
    Packer(ByteBuffer* buf);

    void WriteInt8(INT8 val);
    void WriteInt16(INT16 val);
    void WriteInt24(INT32 val);
    void WriteInt32(INT32 val);
    void WriteInt48(INT64 val);
    void WriteInt64(INT64 val);
    void WriteUInt8(UINT8 val);
    void WriteUInt16(UINT16 val);
    void WriteUInt24(UINT32 val);
    void WriteUInt32(UINT32 val);
    void WriteUInt48(UINT64 val);
    void WriteUInt64(UINT64 val);
    void WriteInt(INT64 val, int bits);
    void WriteUInt(UINT64 val, int bits);
    void WriteBytes(const char* buf, int len);

    void SetUInt(int index, UINT64 val, int bits);

    INT8 ReadInt8();
    INT16 ReadInt16();
    INT32 ReadInt24();
    INT32 ReadInt32();
    INT64 ReadInt48();
    INT64 ReadInt64();
    UINT8 ReadUInt8();
    UINT16 ReadUInt16();
    UINT32 ReadUInt24();
    UINT32 ReadUInt32();
    UINT64 ReadUInt48();
    UINT64 ReadUInt64();
    INT64 ReadInt(int bits);
    UINT64 ReadUInt(int bits);
    int ReadBytes(char* buf, int len);

    void SignedNumToBytes(INT64 val, char* buf, int bytesCount);
    void UnsignedNumToBytes(UINT64 val, char* buf, int bytesCount);
    INT64 BytesToSignedNum(char* buf, int len);
    UINT64 BytesToUnsignedNum(char* buf, int len);

    inline int ReadPosition() const { return m_ROffset; }
    void SetReadPosition(int val) { m_ROffset = val; }

    int WritePosition() { return m_Buffer.Length(); }
    int Available();
    const char* Ptr() { return m_Buffer.Ptr(); }

    void Reset();

private:
    ByteBuffer m_Buffer;
    int        m_ROffset;
    bool       m_BigEndian;

    // temp
    char       m_NumBuf[8];
};


#endif //VTLS_PACKER_H
